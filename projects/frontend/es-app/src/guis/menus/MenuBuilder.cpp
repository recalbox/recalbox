//
// Created by bkg2k on 21/10/24.
//

#include "MenuBuilder.h"
#include "Upgrade.h"
#include "MenuProvider.h"
#include "guis/GuiMsgBox.h"
#include "guis/GuiInfoPopup.h"
#include "MainRunner.h"
#include "guis/menus/tasks/MenuTaskRefreshConnection.h"
#include "guis/menus/tasks/MenuTaskRefreshIp.h"
#include "guis/menus/tasks/MenuTaskRefreshSsid.h"
#include "games/GameFilesUtils.h"
#include "guis/menus/tasks/MenuTaskRefreshAudioOutputs.h"
#include "guis/menus/tasks/MenuTaskSynchronizeVolumes.h"
#include "systems/DownloaderManager.h"
#include "netplay/DefaultPasswords.h"
#include <guis/menus/base/ItemText.h>
#include <guis/menus/base/ItemSelector.h>
#include <guis/menus/base/ItemSlider.h>
#include <guis/menus/base/ItemBar.h>
#include <guis/menus/base/ItemSwitch.h>
#include <guis/menus/base/ItemRating.h>
#include <guis/menus/base/ItemAction.h>
#include <guis/menus/base/ItemEditable.h>
#include <guis/menus/base/ItemSubMenu.h>
#include <systems/SystemManager.h>
#include <patreon/PatronInfo.h>
#include <audio/AudioController.h>

MenuBuilder::~MenuBuilder()
{
  bool reboot = false;
  bool relaunch = false;
  bool bootConf = false;

  // Check reboot/relaunch
  for(const auto& link : mItemsLinks)
    if (link.first->HasChanged())
    {
      reboot |= link.second->Reboot();
      relaunch |= link.second->Relaunch();
      bootConf |= link.second->BootConf();
    }

  if (reboot)
  {
    mWindow.pushGui(new GuiMsgBox(mWindow, _("THE SYSTEM WILL NOW REBOOT"),
                                  _("OK"), []{ MainRunner::RequestQuit(MainRunner::ExitState::NormalReboot); },
                                  _("LATER"), std::bind(MenuBuilder::RebootPending, &mWindow)));
  }
  else if (relaunch)
  {
    mWindow.pushGui(
      new GuiMsgBox(mWindow, _("Recalbox interface must restart to apply your changes."),
                    _("OK"), []{ MainRunner::RequestQuit(MainRunner::ExitState::Relaunch, true); },
                    _("LATER"), std::bind(MenuBuilder::RebootPending, &mWindow)));
  }

  if (bootConf)
  {
    RecalboxSystem::MakeBootReadWrite();
    Files::CopyFile(mConf.FilePath(), Path("/boot/recalbox-backup.conf"));
    RecalboxSystem::MakeBootReadOnly();
  }

  ClearTasks();
}

void MenuBuilder::ClearTasks()
{
  // Stop background tasks
  for(IMenuBackgroundTask* task : mTasks)
  {
    task->TaskStop();
    delete task;
  }
  mTasks.Clear();
}

void MenuBuilder::RebootPending(WindowManager* window)
{
  static bool pending = false;
  if (!pending)
  {
    window->InfoPopupAdd(new GuiInfoPopup(*window, _("A reboot is required to apply pending changes."), 10000, PopupType::Reboot));
    pending = true;
  }
}

void MenuBuilder::BuildThemeOptionSelector(const ItemDefinition& item, const String& selected, const String::List& items)
{
  bool found = false;
  String realSelected;
  for(const String& s : items) if (s == selected) { found = true; realSelected = s; break; }
  if (!found && !items.empty()) realSelected = items.front();

  // Build list
  SelectorEntry<String>::List list;
  for (const String& s : items) list.push_back({ s, s, s == realSelected });
  // Try numeric sorting, else  use an alphanumeric sort
  if (!TrySortNumerically(list))
    std::sort(list.begin(), list.end(), [] (const SelectorEntry<String>& a, const SelectorEntry<String>& b) -> bool { return a.mText < b.mText; });

  ItemSelector<String>* existing = AsList<String>(item.Type());
  if (!items.empty())
  {
    if (existing != nullptr) existing->ChangeSelectionItems(list, realSelected, list.front().mValue).SetSelectable(true);
    else AddList<String>(item, list, realSelected, list.front().mValue, false);
  }
  else
  {
    if (existing != nullptr) existing->ChangeSelectionItems({{ _("OPTION NOT AVAILABLE"), "" }}, "", "").SetSelectable(false);
    else AddList<String>(item, {{ _("OPTION NOT AVAILABLE"), "" }}, "", "", true);
  }
}

bool MenuBuilder::TrySortNumerically(SelectorEntry<String>::List& list)
{
  HashMap<String, int> nameToNumeric;
  for(const SelectorEntry<String>& item : list)
  {
    if (item.mText.empty()) return false;
    size_t pos = item.mText.find_first_not_of("0123456789");
    if (pos == 0) return false;
    nameToNumeric[item.mText] = (pos == std::string::npos) ? item.mText.AsInt() : item.mText.AsInt(item.mText[pos]);
  }

  std::sort(list.begin(), list.end(), [&nameToNumeric] (const SelectorEntry<String>& a, const SelectorEntry<String>& b) -> bool { return nameToNumeric[a.mText] < nameToNumeric[b.mText]; });
  return true;
}

bool MenuBuilder::EvaluateIdentifier(const String& identifier)
{
  typedef bool (*IdentifierMethod)(MenuBuilder*);
  static HashMap<String, IdentifierMethod> sMethodTable
  {
    { "hardware.crt"           , [](MenuBuilder* This) { return This->mResolver.HasCrt(); } },
    { "hardware.crt"           , [](MenuBuilder* This) { return This->mResolver.HasCrt(); } },
    { "hardware.jamma"         , [](MenuBuilder* This) { return This->mResolver.HasJamma(); } },
    { "hardware.rrgbd"         , [](MenuBuilder* This) { return This->mResolver.HasRRGBD(); } },
    { "display.overscan"       , [](MenuBuilder* This) { return This->mResolver.HasCrt() && !This->mResolver.HasJamma(); } },
    { "display.tate"           , [](MenuBuilder* This) { return This->mResolver.IsTate(); } },
    { "display.tateright"      , [](MenuBuilder* This) { return This->mResolver.IsTateRight(); } },
    { "display.tateleft"       , [](MenuBuilder* This) { return This->mResolver.IsTateLeft(); } },
    { "display.qvga"           , [](MenuBuilder* This) { return This->mResolver.IsQVGA(); } },
    { "display.vga"            , [](MenuBuilder* This) { return This->mResolver.IsVGA(); } },
    { "display.hd"             , [](MenuBuilder* This) { return This->mResolver.IsHD(); } },
    { "display.fhd"            , [](MenuBuilder* This) { return This->mResolver.IsFHD(); } },
    { "system.isvirtual"       , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->IsVirtual() : false; } },
    { "system.isarcade"        , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->IsArcade() : false; } },
    { "system.isport"          , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->IsPorts() : false; } },
    { "system.isconsole"       , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Console : false; } },
    { "system.ishandheld"      , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Handheld : false; } },
    { "system.iscomputer"      , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Computer : false; } },
    { "system.isfantasy"       , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Fantasy : false; } },
    { "system.isengine"        , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Engine : false; } },
    { "system.isfavorites"     , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->IsFavorite() : false; } },
    { "system.islastplayed"    , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->IsLastPlayed() : false; } },
    { "system.isscreenshots"   , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->IsScreenshots() : false; } },
    { "system.isalwaysflat"    , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->IsAlwaysFlat() : false; } },
    { "system.isselfsorted"    , [](MenuBuilder* This) { return This->Context().HasSystem() ? This->Context().System()->IsSelfSorted() : false; } },
    { "system.hasdownloader"   , [](MenuBuilder* This) { return This->Context().HasSystem() ? (DownloaderManager::HasDownloader(*This->Context().System())) : false; } },
    { "game.isgame"            , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->IsGame() : false; } },
    { "game.isfolder"          , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->IsFolder() : false; } },
    { "game.isreadonly"        , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->TopAncestor().ReadOnly() : false; } },
    { "game.hassavestate"      , [](MenuBuilder* This) { return This->Context().HasGame() ? !GameFilesUtils::GetGameSaveStateFiles(*This->Context().Game()).empty() : false; } },
    { "game.hassoftpatch"      , [](MenuBuilder* This) { return This->Context().HasGame() ? !GameFilesUtils::GetSoftPatches(This->Context().Game()).empty() : false; } },
    { "game.hassoftpatchcore"  , [](MenuBuilder* This) { return This->Context().HasGame() ? EmulatorManager::GetGameEmulator(*This->Context().Game()).CoreInfo().Softpatching() : false; } },
    { "game.hasmediafiles"     , [](MenuBuilder* This) { return This->Context().HasGame() ? !GameFilesUtils::GetMediaFiles(*This->Context().Game()).empty() : false; } },
    { "game.hasextrafiles"     , [](MenuBuilder* This) { return This->Context().HasGame() ? !GameFilesUtils::GetGameExtraFiles(*This->Context().Game()).empty() : false; } },
    { "game.hassavefiles"      , [](MenuBuilder* This) { return This->Context().HasGame() ? !GameFilesUtils::GetGameSaveFiles(*This->Context().Game()).empty() : false; } },
    { "game.system.isvirtual"  , [](MenuBuilder* This) { (void)This; return false; } },
    { "game.system.isarcade"   , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->System().IsArcade() : false; } },
    { "game.system.isport"     , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->System().IsPorts() : false; } },
    { "game.system.isconsole"  , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->System().Descriptor().Type() == SystemDescriptor::SystemType::Console : false; } },
    { "game.system.ishandheld" , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->System().Descriptor().Type() == SystemDescriptor::SystemType::Handheld : false; } },
    { "game.system.iscomputer" , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->System().Descriptor().Type() == SystemDescriptor::SystemType::Computer : false; } },
    { "game.system.isfantasy"  , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->System().Descriptor().Type() == SystemDescriptor::SystemType::Fantasy : false; } },
    { "game.system.isengine"   , [](MenuBuilder* This) { return This->Context().HasGame() ? This->Context().Game()->System().Descriptor().Type() == SystemDescriptor::SystemType::Engine : false; } },
    { "menu.bartop"            , [](MenuBuilder* This) { return This->mResolver.IsBartopModeOrHigher(); } },
    { "menu.nomenu"            , [](MenuBuilder* This) { return This->mResolver.IsNoMenuMode(); } },
    { "feature.editfavorite"   , [](MenuBuilder* This) { return This->mConf.GetEnableEditFavorites(); } },
    { "feature.bootongame"     , [](MenuBuilder* This) { return This->mConf.GetAutorunEnabled(); } },
    { "feature.kodienabled"    , [](MenuBuilder* This) { return This->mConf.GetKodiEnabled(); } },
    { "feature.kodiexists"     , [](MenuBuilder* This) { (void)This; return RecalboxSystem::kodiExists(); } },
    { "storage.hasextradevices", [](MenuBuilder* This) { return This->mProvider.SystemManager().GetMountMonitor().AllMountPoints().size() > 1; } },
    { "state.ispatron"         , [](MenuBuilder* This) { (void)This; return PatronInfo::Instance().IsPatron(); } },
    { "state.isbeta"           , [](MenuBuilder* This) { return This->mBeta; } },
    { "board.haspowerbutton"   , [](MenuBuilder* This) { (void)This; return !Case::CurrentCase().CanShutdownFromMenu(); } },
    { "board.ispcboard"        , [](MenuBuilder* This) { (void)This; return Board::Instance().IsPC(); } },
    { "board.ispiboard"        , [](MenuBuilder* This) { (void)This; return Board::Instance().IsPi(); } },
    { "board.ispi02board"      , [](MenuBuilder* This) { (void)This; return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::Pi02; }); } },
    { "board.ispi3board"       , [](MenuBuilder* This) { (void)This; return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::Pi3 || t == BoardType::Pi3plus; }); } },
    { "board.ispi4board"       , [](MenuBuilder* This) { (void)This; return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::Pi4 || t == BoardType::Pi400; }); } },
    { "board.ispi5board"       , [](MenuBuilder* This) { (void)This; return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::Pi5 || t == BoardType::Pi500; }); } },
    { "feature.hasvulkan"      , [](MenuBuilder* This) { (void)This; return Board::Instance().HasVulkanSupport(); } },
    { "board.hasbrightness"    , [](MenuBuilder* This) { (void)This; return Board::Instance().HasBrightnessSupport(); } },
    { "board.handheldboard"    , [](MenuBuilder* This) { (void)This; return Board::Instance().IsHandheldSystem(); } },
    { "board.homeboard"        , [](MenuBuilder* This) { (void)This; return Board::Instance().IsHomeSystem(); } },
    { "case"                   , [](MenuBuilder* This) { (void)This; return Case::SupportedManualCases().empty(); } },
  };

  IdentifierMethod* method = sMethodTable.try_get(identifier);
  if (method != nullptr) return (*method)(this);

  { LOGT(LogDebug) << "[MenuBuilder] Unknown identifier " << identifier << ". False evaluation assumed."; }
  return false;
}

void MenuBuilder::BuildMenuItems()
{
  // Clear previous items if any
  mItemsTypes.clear();
  mItemsLinks.clear();
  ClearTasks();

  // Create items
  for(const ItemDefinition& item : mDefinition.mItems)
  {
    // Add item
    switch (item.Category())
    {
      case ItemDefinition::ItemCategory::SubMenu:
      {
        // Condition?
        if (item.HasCondition() && !SimpleExpressionEvaluator(*this).Evaluate(item.Condition())) break;
        // Custom create?
        if (!AddCustomSubMenu(item))
        {
          ItemSubMenu* submenu = (item.Icon() != MenuThemeData::MenuIcons::Type::Unknown) ?
                                 Menu::AddSubMenu(item.Caption(this), item.Icon(), (int)item.MenuType(), &mProvider, item.Help(), mProvider.IsMenuUnselectable(item.MenuType()), item.UnselectableHelp()) :
                                 Menu::AddSubMenu(item.Caption(this), (int)item.MenuType(), &mProvider, item.Help(), mProvider.IsMenuUnselectable(item.MenuType()), item.UnselectableHelp());
          // Merge context
          submenu->MergeContext(Context()).ReplaceParameters();
        }
        break;
      }
      case ItemDefinition::ItemCategory::Item:
      {
        // Condition?
        if (item.HasCondition())
          if (!SimpleExpressionEvaluator(*this).Evaluate(item.Condition()))
            break;
        AddItem(item);
        break;
      }
      case ItemDefinition::ItemCategory::Header:
      {
        AddHeader(item.Caption(this));
        break;
      }
      default:
      { LOG(LogFatal) << "[MenuProvider] Unknown menu item !"; }
    }
  }

  if (Count() == 0)
    Menu::AddText("EMPTY MENU", "CHECK MENU.XML!");
  else
    for(IMenuBackgroundTask* task : mTasks)
      task->TaskStart();
}

void MenuBuilder::MenuItemShow(ItemBase& item)
{
  // Refresh system option submenu
  if (item.IsSubMenu() && (MenuContainerType)item.Identifier() == MenuContainerType::AdvancedEmulatorOptionsList)
    item.ChangeLabel(mProvider.SystemNameWithEmulator(item.Context().System()));
}

bool MenuBuilder::AddCustomSubMenu(const ItemDefinition& subMenuItem)
{
  switch(subMenuItem.MenuType())
  {
    case MenuContainerType::StorageDevice:
    {
      for(const StorageDevices::Device& device : StorageDevices().GetStorageDevices())
      {
        String p1;
        switch(device.Type)
        {
          case StorageDevices::Types::Internal: continue;
          case StorageDevices::Types::Ram: continue;
          case StorageDevices::Types::Network: continue;
          case StorageDevices::Types::External: p1 = device.DisplayableName(); break;
        }
        AddSubMenu(subMenuItem, false)->MergeContext(Context())
                                      .MergeContext(InheritableContext(&device))
                                      .ReplaceParameters(p1, false);
      }
      return true;
    }
    case MenuContainerType::AdvancedEmulatorOptionsList:
    {
      for(SystemData* system : mProvider.SystemManager().VisibleSystemList())
        if (!system->IsVirtual())
          AddSubMenu(subMenuItem, false)->MergeContext(Context())
                                        .MergeContext(InheritableContext(system))
                                        .ReplaceParameters(mProvider.SystemNameWithEmulator(system), false);
      return true;
    }
    case MenuContainerType::Main:
    case MenuContainerType::SystemSettings:
    case MenuContainerType::StorageList:
    case MenuContainerType::Update:
    case MenuContainerType::Controllers:
    case MenuContainerType::Crt:
    case MenuContainerType::UI:
    case MenuContainerType::Screensaver:
    case MenuContainerType::Theme:
    case MenuContainerType::PopupSettings:
    case MenuContainerType::GameFilters:
    case MenuContainerType::Lists:
    case MenuContainerType::InGame:
    case MenuContainerType::NetplayPasswords:
    case MenuContainerType::Arcade:
    case MenuContainerType::Tate:
    case MenuContainerType::Advanced:
    case MenuContainerType::BootSettings:
    case MenuContainerType::VirtualSystems:
    case MenuContainerType::Resolutions:
    case MenuContainerType::AdvancedEmulatorOptions:
    case MenuContainerType::Kodi:
    case MenuContainerType::Sound:
    case MenuContainerType::Network:
    case MenuContainerType::Scraper:
    case MenuContainerType::Download:
    case MenuContainerType::Gamelist:
    case MenuContainerType::Quit:
    case MenuContainerType::ChooseKodiLobby:
    case MenuContainerType::UserScripts:
    case MenuContainerType::EditGame:
    case MenuContainerType::DeleteOptions:
    case MenuContainerType::DeleteAdvanced:
    case MenuContainerType::SelectSoftpatch:
    case MenuContainerType::NetplayHost:
    case MenuContainerType::NetplayClient:
      break;
    case MenuContainerType::_Error_: { LOG(LogFatal) << "[MenuBuilder] Pretty impossible to see this error :)"; }
  }
  return false;
}

void MenuBuilder::AddItem(const ItemDefinition& item)
{
  bool isBeta =
    #if defined(BETA) || defined(DEBUG)
    true
    #else
    false
  #endif
  ;

  // Get default grayed state
  bool defaultGrayed = false;
  if (item.HasGrayedCondition())
    if (SimpleExpressionEvaluator(*this).Evaluate(item.GrayedCondition()))
      defaultGrayed = true;

  // Build items
  switch(item.Type())
  {
    // Version & platform
    case MenuItemType::RecalboxVersion: { AddText(item, mProvider.GetVersionString()); break; }
    case MenuItemType::UsedShare:
    {
      const StorageDevices::Device& share = StorageDevices().GetShareDevices().front();
      String text = _S((_F("{0} free of {1}") / share.HumanFree() / share.HumanSize())());
      AddBar(item, text, (float)share.Free / (float)share.Size, defaultGrayed);
      break;
    }
    case MenuItemType::ShareDevice:
    {
      // Storage device
      int currentIndex = 0;
      SelectorEntry<StorageDevices::Device>::List list = mDataProvider.GetStorageEntries(currentIndex);
      if (!list.empty())
        AddList<StorageDevices::Device>(item, list, list[currentIndex].mValue, list[currentIndex].mValue, false);
      break;
    }
    case MenuItemType::DeviceList: break;
    case MenuItemType::Language: { AddList<String>(item, mDataProvider.GetCultureEntries(), mConf.GetSystemLanguage(), "en_US", defaultGrayed); break; }
    case MenuItemType::TimeZone: { AddList<String>(item, mDataProvider.GetTimeZones(), mConf.GetSystemTimezone(), "UTC", defaultGrayed); break; }
    case MenuItemType::Keyboard: { AddList<String>(item, mDataProvider.GetKeyboardEntries(), mConf.GetSystemKbLayout(), "us", defaultGrayed); break; }
    case MenuItemType::Shutdown: /* fallthrough to Restart */
    case MenuItemType::FastShutdown: /* fallthrough to Restart */
    case MenuItemType::Restart: { AddAction(item, "", true, defaultGrayed); break; }
    case MenuItemType::RunKodi: { AddAction(item, "RUN", true, defaultGrayed); break; }
    case MenuItemType::OpenLobby: { AddAction(item, "OPEN", true, defaultGrayed); break; }
    case MenuItemType::EnableKodi: { AddSwitch(item, mConf.GetKodiEnabled(), defaultGrayed); break; }
    case MenuItemType::KodiResolution: { AddList<String>(item, mDataProvider.GetKodiResolutionsEntries(), !mConf.IsDefinedKodiVideoMode() ? String::Empty : mConf.GetKodiVideoMode(), String::Empty, defaultGrayed); break; }
    case MenuItemType::KodiOnStartup: { AddSwitch(item, mConf.GetKodiAtStartup(), defaultGrayed); break; }
    case MenuItemType::KodiOnX: { AddSwitch(item, mConf.GetKodiXButton(), defaultGrayed); break; }
    case MenuItemType::CheckBios: { AddAction(item, "CHECK", true, defaultGrayed); break; }
    case MenuItemType::ShowLicense: { AddAction(item, "SHOW", true, defaultGrayed); break; }
    case MenuItemType::ScrapeFrom: { AddList<ScraperType>(item, mDataProvider.GetScrapersEntries(), mConf.GetScraperSource(), ScraperType::ScreenScraper, defaultGrayed); break; }
    case MenuItemType::ScrapeAuto: { AddSwitch(item, mConf.GetScraperAuto(), defaultGrayed); break; }
    case MenuItemType::ScrapeOptionNameFrom: { AddList<ScraperNameOptions>(item, mDataProvider.GetScraperNameOptionsEntries(), mConf.GetScraperNameOptions(), ScraperNameOptions::GetFromScraper, defaultGrayed); break; }
    case MenuItemType::ScrapeOptionImage: { AddList<ScreenScraperEnums::ScreenScraperImageType>(item, mDataProvider.GetScraperImagesEntries(), mConf.GetScreenScraperMainMedia(), ScreenScraperEnums::ScreenScraperImageType::MixV2, defaultGrayed); break; }
    case MenuItemType::ScrapeOptionVideo: { AddList<ScreenScraperEnums::ScreenScraperVideoType>(item, mDataProvider.GetScraperVideosEntries(), mConf.GetScreenScraperVideo(), ScreenScraperEnums::ScreenScraperVideoType::None, defaultGrayed); break; }
    case MenuItemType::ScrapeOptionThumb: { AddList<ScreenScraperEnums::ScreenScraperImageType>(item, mDataProvider.GetScraperThumbnailsEntries(), mConf.GetScreenScraperThumbnail(), ScreenScraperEnums::ScreenScraperImageType::None, defaultGrayed); break; }
    case MenuItemType::ScrapeOptionRegionPriority: { AddList<ScreenScraperEnums::ScreenScraperRegionPriority>(item, mDataProvider.GetScraperRegionOptionsEntries(), mConf.GetScreenScraperRegionPriority(), ScreenScraperEnums::ScreenScraperRegionPriority::DetectedRegion, defaultGrayed); break; }
    case MenuItemType::ScrapeOptionRegionFavorite: { AddList<Regions::GameRegions>(item, mDataProvider.GetScraperRegionsEntries(), mConf.GetScreenScraperRegion(), Regions::GameRegions::WOR, defaultGrayed); break; }
    case MenuItemType::ScrapeOptionLanguageFavorite: { AddList<Languages>(item, mDataProvider.GetScraperLanguagesEntries(), LanguagesTools::GetScrapingLanguage(), Languages::EN, defaultGrayed); break; }
    case MenuItemType::ScrapeOptionManual: { AddSwitch(item, mConf.GetScreenScraperWantManual(), defaultGrayed); break; }
    case MenuItemType::ScrapeOptionMap: { AddSwitch(item, mConf.GetScreenScraperWantMaps(), defaultGrayed); break; }
    case MenuItemType::ScrapeOptionP2K: { AddSwitch(item, mConf.GetScreenScraperWantP2K(), defaultGrayed); break; }
    case MenuItemType::ScrapeOptionUsername:
    {
      if (!mItemsTypes.contains(MenuItemType::ScrapeFrom)) { LOG(LogError) << "[MenuBuilder] MenuItemType::ScrapeOptionUsername must be created AFTER MenuItemType::ScrapeFrom."; }
      ScraperType type = mItemsTypes[MenuItemType::ScrapeFrom]->AsList<ScraperType>()->SelectedValue();
      AddEditor(item, ScraperFactory::GetLogin(type), false, !ScraperFactory::HasCredentials(type));
      break;
    }
    case MenuItemType::ScrapeOptionPassword:
    {
      if (!mItemsTypes.contains(MenuItemType::ScrapeFrom)) { LOG(LogError) << "[MenuBuilder] MenuItemType::ScrapeOptionPassword must be created AFTER MenuItemType::ScrapeFrom."; }
      ScraperType type = mItemsTypes[MenuItemType::ScrapeFrom]->AsList<ScraperType>()->SelectedValue();
      AddEditor(item, ScraperFactory::GetPassword(type), false, !ScraperFactory::HasCredentials(type));
      break;
    }
      case MenuItemType::ScrapeFilter: { AddList<ScrapingMethod>(item, mDataProvider.GetScrapingMethods(), ScraperFactory::Instance().ScrapingMethod(), ScrapingMethod::AllIfNothingExists, defaultGrayed); break; }
    case MenuItemType::ScrapeSystems: { AddMultiList<SystemData*>(item, mDataProvider.GetScrapableSystemsEntries(), defaultGrayed); break; }
    case MenuItemType::ScrapeRun: { AddAction(item, "RUN", true, defaultGrayed); break; }
    case MenuItemType::Rewind: { AddSwitch(item, mConf.GetGlobalRewind(), defaultGrayed); break; }
    case MenuItemType::SoftPatching: { AddList<RecalboxConf::SoftPatching>(item, mDataProvider.GetSoftpatchingEntries(), mConf.GetGlobalSoftpatching(), RecalboxConf::SoftPatching::Disable, defaultGrayed); break; }
    case MenuItemType::ShowSaveStates: { AddSwitch(item, mConf.GetGlobalShowSaveStateBeforeRun(), defaultGrayed); break; }
    case MenuItemType::AutoSave: { AddSwitch(item, mConf.GetGlobalAutoSave(), defaultGrayed); break; }
    case MenuItemType::PressTwice: { AddSwitch(item, mConf.GetGlobalQuitTwice(), defaultGrayed); break; }
    case MenuItemType::SuperGameboyMode: { AddList<String>(item, mDataProvider.GetSuperGameBoyEntries(), mConf.GetSuperGameBoy(), "gb", defaultGrayed); break; }
    case MenuItemType::GameRatio: { AddList<String>(item, mDataProvider.GetRatioEntries(), mConf.GetGlobalRatio(), "auto", defaultGrayed); break; }
    case MenuItemType::RecalboxOverlays: { AddSwitch(item, mConf.GetGlobalRecalboxOverlays(), defaultGrayed); break; }
    case MenuItemType::Smooth: { AddSwitch(item, mConf.GetGlobalSmooth(), defaultGrayed); break;}
    case MenuItemType::IntegerScale: { AddSwitch(item, mConf.GetGlobalIntegerScale(), defaultGrayed); break; }
    case MenuItemType::ShaderSet: { AddList<String>(item, mDataProvider.GetShaderPresetsEntries(), mConf.GetGlobalShaderSet(), "none", defaultGrayed); break; }
    case MenuItemType::AdvancedShaders:{ AddList<String>(item, mDataProvider.GetShadersEntries(), mConf.GetGlobalShaders(), "", defaultGrayed); break; }
    case MenuItemType::HDMode:{ AddSwitch(item, mConf.GetGlobalHDMode(), defaultGrayed); break; }
    case MenuItemType::WideScreen: { AddSwitch(item, mConf.GetGlobalWidescreenMode(), defaultGrayed); break; }
    case MenuItemType::VulkanDriver: { AddSwitch(item, mConf.GetGlobalVulkanDriver(), defaultGrayed); break; }
    case MenuItemType::Retroachievements: { AddSwitch(item, mConf.GetRetroAchievementOnOff(), defaultGrayed); break; }
    case MenuItemType::RetroachievementsHardcore: { AddSwitch(item, mConf.GetRetroAchievementHardcore(), defaultGrayed); break; }
    case MenuItemType::RetroachievementsUsername: { AddEditor(item, mConf.GetRetroAchievementLogin(), false, defaultGrayed); break; }
    case MenuItemType::RetroachievementsPassword: { AddEditor(item, mConf.GetRetroAchievementPassword(), true, defaultGrayed); break; }
    case MenuItemType::Netplay: { AddSwitch(item, mConf.GetNetplayEnabled(), defaultGrayed); break; }
    case MenuItemType::NetplayNickname: { AddEditor(item, mConf.GetNetplayLogin(), false, defaultGrayed); break; }
    case MenuItemType::NetplayPort: { AddEditor(item, String(mConf.GetNetplayPort()), false, defaultGrayed); break; }
    case MenuItemType::NetplayRelay: { AddList<RecalboxConf::Relay>(item, mDataProvider.GetMitmEntries(), mConf.GetNetplayRelay(), RecalboxConf::Relay::None, defaultGrayed); break; }
    case MenuItemType::HashRoms: { AddAction(item, "RUN", true, defaultGrayed); break; }
    case MenuItemType::UpdateCheck: { AddSwitch(item, mConf.GetUpdatesEnabled(), defaultGrayed); break; }
    case MenuItemType::UpdateAvailable: { AddText(item, Upgrade::Instance().PendingUpdate() ? _("YES").Append(" (").Append(Upgrade::Instance().NewVersion()).Append(')') : _("NO")); break; }
    case MenuItemType::UpdateChangelog: { AddAction(item, "SHOW", true, !Upgrade::Instance().PendingUpdate()); break; }
    case MenuItemType::UpdateStart: { AddAction(item, "GO!", true, !Upgrade::Instance().PendingUpdate()); break; }
    case MenuItemType::UpdateType: { if (isBeta || PatronInfo::Instance().IsPatron()) AddList<RecalboxConf::UpdateType>(item, mDataProvider.GetUpdateTypeEntries(), mConf.GetUpdateType(), RecalboxConf::UpdateType::Stable, defaultGrayed); break; }
    case MenuItemType::UpdateCheckNow: { AddAction(item, _("CHECK"), true, defaultGrayed); break; }
    case MenuItemType::NetworkStatus: { AddTask(new MenuTaskRefreshConnection(*AddText(item, _("NOT CONNECTED")))); break; }
    case MenuItemType::NetworkIP: { AddTask(new MenuTaskRefreshIP(*AddText(item, _("UNAVAILABLE")))); break; }
    case MenuItemType::NetworkHostname: { AddEditor(item, mConf.GetHostname(), false, defaultGrayed); break; }
    case MenuItemType::NetworkEnableWIFI: { AddSwitch(item, mConf.GetWifiEnabled(), defaultGrayed); break; }
    case MenuItemType::NetworkPickSSID: { AddTask(new MenuTaskRefreshSSID(*AddList<String>(item, { { _("NO WIFI ACCESS POINT"), String::Empty } }, String::Empty, String::Empty, defaultGrayed))); break; }
    case MenuItemType::NetworkSSID: { AddEditor(item, mConf.GetWifiSSID(), false, defaultGrayed); break; }
    case MenuItemType::NetworkPassword: { AddEditor(item, mConf.GetWifiKey(), true, defaultGrayed); break; }
    case MenuItemType::NetworkWPS: { AddAction(item, _("CONNECT"), true, defaultGrayed); break; }
    case MenuItemType::ShowOnlyLastVersions: { AddSwitch(item, mConf.GetShowOnlyLatestVersion(), defaultGrayed); break; }
    case MenuItemType::ShowOnlyFavorites: { AddSwitch(item, mConf.GetFavoritesOnly(), defaultGrayed); break; }
    case MenuItemType::ShowFavoriteFirst: { AddSwitch(item, mConf.GetFavoritesFirst(), defaultGrayed); break; }
    case MenuItemType::ShowHiddenGames: { AddSwitch(item, mConf.GetShowHidden(), defaultGrayed); break; }
    case MenuItemType::ShowMahjongCasino: { AddSwitch(item, !mConf.GetHideBoardGames(), defaultGrayed); break; }
    case MenuItemType::ShowAdultGames: { AddSwitch(item, !mConf.GetFilterAdultGames(), defaultGrayed); break; }
    case MenuItemType::ShowPreinstalledGames:  { AddSwitch(item, !mConf.GetGlobalHidePreinstalled(), defaultGrayed); break; }
    case MenuItemType::Show3PlayerGames: { AddSwitch(item, mConf.GetShowOnly3PlusPlayers(), defaultGrayed); break; }
    case MenuItemType::ShowOnlyYoko: { AddSwitch(item, mConf.GetShowOnlyYokoGames(), defaultGrayed); break; }
    case MenuItemType::ShowOnlyTate: { AddSwitch(item, mConf.GetShowOnlyTateGames(), defaultGrayed); break; }
    case MenuItemType::ShowNonGames: { AddSwitch(item, !mConf.GetHideNoGames(), defaultGrayed); break; }
    case MenuItemType::ScreensaverTimeout: { AddSlider(item, 0.f, 30.f, 1.f, 5.f,  (float)mConf.GetScreenSaverTime(), "m", defaultGrayed); break; }
    case MenuItemType::ScreensaverType: { AddList<RecalboxConf::Screensaver>(item, mDataProvider.GetTypeEntries(), mConf.GetScreenSaverType(), RecalboxConf::Screensaver::Dim, defaultGrayed); break; }
    case MenuItemType::ScreensaverSystems: { AddMultiList<String>(item, mDataProvider.GetSystemListAsString(), defaultGrayed); break; }
    case MenuItemType::PopupHelpDuration: { AddSlider(item, 0.f, 10.f, 1.f, 10.f, (float)mConf.GetPopupHelp(), "s", defaultGrayed); break; }
    case MenuItemType::PopupMusic: { AddSlider(item, 0.f, 10.f, 1.f, 5.f, (float)mConf.GetPopupMusic(), "s", defaultGrayed); break; }
    case MenuItemType::PopupNetplay: { AddSlider(item, 0.f, 10.f, 1.f, 8.f, (float)mConf.GetPopupNetplay(), "s", defaultGrayed); break; }
    case MenuItemType::ThemeSet: { AddList<ThemeSpec>(item, mDataProvider.GetThemeEntries(mWindow), mDataProvider.GetCurrentTheme(), mDataProvider.GetDefaultTheme(), defaultGrayed); break; }
    case MenuItemType::ThemeCarousel: { AddSwitch(item, mConf.GetThemeCarousel(), defaultGrayed); break; }
    case MenuItemType::ThemeTransition: { AddList<String>(item, mDataProvider.GetThemeTransitionEntries(), mConf.GetThemeTransition(), "slide", defaultGrayed); break; }
    case MenuItemType::ThemeRegion: { AddList<String>(item, mDataProvider.GetThemeRegionEntries(), mConf.GetThemeRegion(), "us", defaultGrayed); break; }
    case MenuItemType::ThemeOptColorset: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("colorset")); break; }
    case MenuItemType::ThemeOptIconset: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("iconset")); break; }
    case MenuItemType::ThemeOptMenu: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("menu")); break; }
    case MenuItemType::ThemeOptSystemView: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("systemview")); break; }
    case MenuItemType::ThemeOptGameView: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("gamelistview")); break; }
    case MenuItemType::ThemeOptGameClip: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("gameclipview")); break; }
    case MenuItemType::Brightness: { AddSlider(item, 0.f, 8.f, 1.f, 6.f, (float)mConf.GetBrightness(), "", defaultGrayed); break; }
    case MenuItemType::SystemSorting: { AddList<SystemSorting>(item, mDataProvider.GetSystemSortingEntries(), mConf.GetSystemSorting(), SystemSorting::Default, defaultGrayed); break; }
    case MenuItemType::QuickSelectSystem: { AddSwitch(item, mConf.GetQuickSystemSelect(), defaultGrayed); break; }
    case MenuItemType::OnScreenHelp: { AddSwitch(item, mConf.GetShowHelp(), defaultGrayed); break; }
    case MenuItemType::SwapValidateCancel: { AddSwitch(item, mConf.GetSwapValidateAndCancel(), defaultGrayed); break; }
    case MenuItemType::OSDClock: { AddSwitch(item, mConf.GetClock(), defaultGrayed); break; }
    case MenuItemType::DisplayByFilename: { AddSwitch(item, mConf.GetDisplayByFileName(), defaultGrayed); break; }
    case MenuItemType::UpdateGamelists: { AddAction(item, _("UPDATE!"), true, defaultGrayed); break; }
    case MenuItemType::HideSystems: { AddMultiList<SystemData*>(item, mDataProvider.GetHidableSystems(), defaultGrayed); break; }
    case MenuItemType::ArcadeEnhancedView: { AddSwitch(item, mConf.GetArcadeViewEnhanced(), defaultGrayed); break; }
    case MenuItemType::ArcadeFoldClones: { AddSwitch(item, mConf.GetArcadeViewFoldClones(), defaultGrayed); break; }
    case MenuItemType::ArcadeHideBios: { AddSwitch(item, mConf.GetArcadeViewHideBios(), defaultGrayed); break; }
    case MenuItemType::ArcadeHideNonWorking: { AddSwitch(item, mConf.GetArcadeViewHideNonWorking(), defaultGrayed); break; }
    case MenuItemType::ArcadeUseOfficielNames: { AddSwitch(item, mConf.GetArcadeUseDatabaseNames(), defaultGrayed); break; }
    case MenuItemType::ArcadeManufacturerSystems: { AddMultiList<String>(item, mDataProvider.GetManufacturersVirtualEntries(), defaultGrayed); break; }
    case MenuItemType::ArcadeAggregateEnable: { AddSwitch(item, mConf.GetCollectionArcade(), defaultGrayed); break; }
    case MenuItemType::ArcadeAggregateNeoGeo: { AddSwitch(item, mConf.GetCollectionArcadeNeogeo(), defaultGrayed); break; }
    case MenuItemType::ArcadeAggregateOriginal: { AddSwitch(item, mConf.GetCollectionArcadeHideOriginals(), defaultGrayed); break; }
    case MenuItemType::BootOnKodi: { AddSwitch(item, mConf.GetKodiAtStartup(), defaultGrayed); break; }
    case MenuItemType::BootDoNotScan: { AddSwitch(item, mConf.GetStartupGamelistOnly(), defaultGrayed); break; }
    case MenuItemType::AllowBootOnGame: { AddSwitch(item, mConf.GetAutorunEnabled(), defaultGrayed); break; }
    case MenuItemType::BootOnSystem: { AddList<String>(item, mDataProvider.GetBootSystemEntries(), mConf.GetStartupSelectedSystem(), SystemManager::sFavoriteSystemShortName, defaultGrayed); break; }
    case MenuItemType::BootOnGamelist: { AddSwitch(item, mConf.GetStartupStartOnGamelist(), defaultGrayed); break; }
    case MenuItemType::BootShowVideo: { AddSwitch(item, mConf.GetSplashEnabled(), defaultGrayed); break; }
    case MenuItemType::HideSystemView: { AddSwitch(item, mConf.GetStartupHideSystemView(), defaultGrayed); break; }
    case MenuItemType::VirtualAllGames: { AddSwitch(item, mConf.GetCollectionAllGames(), defaultGrayed); break; }
    case MenuItemType::VirtualMultiplayer: { AddSwitch(item, mConf.GetCollectionMultiplayer(), defaultGrayed); break; }
    case MenuItemType::VirtualLastPlayed: { AddSwitch(item, mConf.GetCollectionLastPlayed(), defaultGrayed); break; }
    case MenuItemType::VirtualLightgun: { AddSwitch(item, mConf.GetCollectionLightGun(), defaultGrayed); break; }
    case MenuItemType::VirtualPorts: { AddSwitch(item, mConf.GetCollectionPorts(), defaultGrayed); break;}
    case MenuItemType::VirtualPerGenre: { AddMultiList<GameGenres>(item, mDataProvider.GetMultiGenreEntries(), defaultGrayed); break; }
    case MenuItemType::AdvDebugLogs: { AddSwitch(item, mConf.GetDebugLogs(), defaultGrayed); break; }
    case MenuItemType::AdvShowFPS: { AddSwitch(item, mConf.GetGlobalShowFPS(), defaultGrayed); break; }
    case MenuItemType::ResolutionGlobal: { AddList<String>(item, mDataProvider.GetGlobalResolutionEntries(), mConf.GetGlobalVideoMode(), ResolutionAdapter().DefaultResolution().ToString(), defaultGrayed); break; }
    case MenuItemType::ResolutionFrontEnd: { AddList<String>(item, mDataProvider.GetResolutionEntries(), mConf.GetEmulationstationVideoMode(), String::Empty, defaultGrayed); break; }
    case MenuItemType::ResolutionEmulators:
    {
      SelectorEntry<String>::List resolutions = mDataProvider.GetResolutionEntries();
      for(SystemData* system : mProvider.VisibleSystems())
        AddList<String>(item, resolutions, mConf.GetSystemVideoModeNoDefault(*system), String::Empty, defaultGrayed, true)->ReplaceParameters(system->FullName(), true).MergeContext(InheritableContext(system));
      break;
    }
    case MenuItemType::DeviceFreeSpace: { AddBar(item, (_F("{0} FREE") / Context().Device()->HumanFree())(), 1.0f - (float)((double)Context().Device()->Free / (double)Context().Device()->Size), defaultGrayed); break; }
    case MenuItemType::DeviceName: { AddText(item, Context().Device()->DisplayableShortName())->Parent().SetTitle(Context().Device()->DisplayableShortName()); break; }
    case MenuItemType::DeviceFS: { AddText(item, Context().Device()->FileSystem); break; }
    case MenuItemType::DeviceNetworkAccess: { AddText(item, Context().Device()->MountPoint.empty() ? _("NO ACCESS") : String("\\\\RECALBOX\\share\\externals\\").Append(Path(Context().Device()->MountPoint).Filename())); break; }
    case MenuItemType::DeviceUsable: { AddText(item, Context().Device()->IsNTFS() ? _("YES, BUT NOT RECOMMENDED") : Context().Device()->RecalboxCompatible() ? _("YES") : _("NO")); break; }
    case MenuItemType::DeviceInitialize:
    {
      bool alreadyInitialized = mProvider.SystemManager().HasRomStructure(Path(Context().Device()->MountPoint));
      AddAction(item, _("INITIALIZE!"), true, !(Context().Device()->RecalboxCompatible() && !alreadyInitialized)); break;
    }
    case MenuItemType::WebManagerEnable: { AddSwitch(item, mConf.GetSystemManagerEnabled(), defaultGrayed); break; }
    case MenuItemType::FactoryReset: { AddAction(item, _("RESET!"), true, defaultGrayed); break; }
    case MenuItemType::EmulatorsReset: { AddAction(item, _("RESET!"), true, defaultGrayed); break; }
    case MenuItemType::Overclock: { AddList<Overclocking::Overclock>(item, mDataProvider.GetOverclockEntries(), Board::Instance().GetOverclocking().GetCurrentOverclock(), Board::Instance().GetOverclocking().NoOverclock(), defaultGrayed); break; }
    case MenuItemType::PerSystemEmulator: { String emulatorAndCore; String defaultEmulatorAndCore; AddList(item, mDataProvider.GetEmulatorEntries(Context().System(), emulatorAndCore, defaultEmulatorAndCore), emulatorAndCore, defaultEmulatorAndCore, defaultGrayed); break; }
    case MenuItemType::PerSystemRatio: { AddList(item, mDataProvider.GetRatioEntries(), mConf.GetSystemRatio(*Context().System()), String::Empty, defaultGrayed); break; }
    case MenuItemType::PerSystemSmooth: { AddSwitch(item, mConf.GetSystemSmooth(*Context().System()), defaultGrayed); break; }
    case MenuItemType::PerSystemRewind: { AddSwitch(item, mConf.GetSystemRewind(*Context().System()), defaultGrayed); break; }
    case MenuItemType::PerSystemAutoLoadSave: { AddSwitch(item, mConf.GetSystemAutoSave(*Context().System()), defaultGrayed); break; }
    case MenuItemType::PerSystemShaderSet: { AddList(item, mDataProvider.GetShadersEntries(), mConf.GetSystemShaderSet(*Context().System()), String::Empty, defaultGrayed); break; }
    case MenuItemType::PerSystemShaders: { AddList(item, mDataProvider.GetShaderPresetsEntries(), mConf.GetSystemShaders(*Context().System()), String("none"), defaultGrayed); break; }
    case MenuItemType::BootloaderUpdate: { AddAction(item, _("UPDATE!"), true, defaultGrayed); break; }
    case MenuItemType::CrtAdapter: { AddList<CrtAdapterType>(item, mDataProvider.GetDacEntries(), mResolver.HasRRGBD() ? CrtAdapterType::RGBDual : CrtConf::Instance().GetSystemCRT(), CrtAdapterType::None, defaultGrayed); break; }
    case MenuItemType::CrtMenuResolution: { AddList<String>(item, mDataProvider.GetEsResolutionEntries(), mCrtConf.GetSystemCRTResolution(), "0", defaultGrayed); break; }
    case MenuItemType::CrtScreenType:
    {
      // Polymorphic item
      if (mResolver.HasJamma()) AddList<ICrtInterface::HorizontalFrequency>(item, mDataProvider.GetHorizontalOutputFrequency(), Board::Instance().CrtBoard().GetHorizontalFrequency(), ICrtInterface::HorizontalFrequency::KHz15, false);
      else AddText(item, ICrtInterface::HorizontalFrequencyToHumanReadable(Board::Instance().CrtBoard().GetHorizontalFrequency()));
      break;
    }
    case MenuItemType::CrtForce50hz:
    {
      String result(Board::Instance().CrtBoard().MustForce50Hz() ? _("ON") : _("OFF")); result.Append(' ').Append(_("(Hardware managed)"));
      if (Board::Instance().CrtBoard().HasForced50hzSupport()) AddText(item, result);
      break;
    }
    case MenuItemType::CrtHDMIPriority: { AddSwitch(item, mCrtConf.GetSystemCRTForceHDMI(), defaultGrayed); break; }
    case MenuItemType::CrtSelectRefreshAtLaunch: { AddSwitch(item, mCrtConf.GetSystemCRTGameRegionSelect(), defaultGrayed); break; }
    case MenuItemType::CrtSelectResolutionAtLaunch: { AddSwitch(item, mCrtConf.GetSystemCRTGameResolutionSelect(), defaultGrayed); break; }
    case MenuItemType::CrtSuperrezMultiplier: { AddList<String>(item, mDataProvider.GetSuperRezEntries(), mCrtConf.GetSystemCRTSuperrez(), "1920",  defaultGrayed); break; }
    case MenuItemType::CrtForceJack: { AddSwitch(item, mCrtConf.GetSystemCRTForceJack(), defaultGrayed); break; }
    case MenuItemType::CrtScanline240in480: { AddList<CrtScanlines>(item, mDataProvider.GetScanlinesEntries(), mCrtConf.GetSystemCRTScanlines31kHz(), CrtScanlines::None, Board::Instance().CrtBoard().GetHorizontalFrequency() < ICrtInterface::HorizontalFrequency::KHz31); break;}
    case MenuItemType::CrtDemo240: { AddSwitch(item, mCrtConf.GetSystemCRTRunDemoIn240pOn31kHz(), Board::Instance().CrtBoard().GetHorizontalFrequency() < ICrtInterface::HorizontalFrequency::KHz31); break; }
    case MenuItemType::JammaSound: { AddList<String>(item, { { "JACK/MONO", "0" }, { "JACK/PINS", "1" } }, CrtConf::Instance().GetSystemCRTJammaAmpDisable() ? "1" : "0", "0", defaultGrayed); break; }
    case MenuItemType::JammaAmpBoost: { AddList<String>(item, { { "default", "0" }, { "+6dB", "1" }, { "+12dB", "2" }, { "+16dB", "3" } }, CrtConf::Instance().GetSystemCRTJammaMonoAmpBoost(), "0", defaultGrayed); break;}
    case MenuItemType::JammaPanelType: { AddList<String>(item, { { "2 buttons", "2" }, { "3 buttons", "3" }, { "4 buttons", "4" }, { "5 buttons", "5" }, { "6 buttons", "6" } }, CrtConf::Instance().GetSystemCRTJammaPanelButtons(), "2", defaultGrayed); break; }
    case MenuItemType::JammaNeogeoLayoutP1: { AddList<String>(item, { { "Default", "neodefault" }, { "Line", "line" }, { "Square", "square" } }, CrtConf::Instance().GetSystemCRTJammaNeogeoLayoutP1(), "neodefault", defaultGrayed); break; }
    case MenuItemType::JammaNeogeoLayoutP2: { AddList<String>(item, { { "Default", "neodefault" }, { "Line", "line" }, { "Square", "square" } }, CrtConf::Instance().GetSystemCRTJammaNeogeoLayoutP2(), "neodefault", defaultGrayed); break; }
    case MenuItemType::Jamma4PlayerMode: { AddSwitch(item, mCrtConf.GetSystemCRTJamma4Players(), defaultGrayed); break; }
    case MenuItemType::JammaCredit: { AddSwitch(item, mCrtConf.GetSystemCRTJammaStartBtn1Credit(), defaultGrayed); break; }
    case MenuItemType::JammaHotkey: { AddSwitch(item, mCrtConf.GetSystemCRTJammaHKOnStart(), defaultGrayed); break; }
    case MenuItemType::JammaVolume: { AddSwitch(item, mCrtConf.GetSystemCRTJammaSoundOnStart(), defaultGrayed); break; }
    case MenuItemType::JammaExit: { AddSwitch(item, mCrtConf.GetSystemCRTJammaExitOnStart(), defaultGrayed); break; }
    case MenuItemType::JammaAutoFire: { AddSwitch(item, mCrtConf.GetSystemCRTJammaAutoFire(), defaultGrayed); break; }
    case MenuItemType::JammaDualJoystick: { AddSwitch(item, mCrtConf.GetSystemCRTJammaDualJoysticks(), defaultGrayed); break; }
    case MenuItemType::JammaPinE27: { AddSwitch(item, mCrtConf.GetSystemCRTJammaButtonsOnJamma() != "6", defaultGrayed); break; }
    case MenuItemType::JammaReset: { AddAction(item, _("RESET"), true, defaultGrayed); break; }
    case MenuItemType::CrtReducedLantency: { AddSwitch(item, mConf.GetGlobalReduceLatency(), defaultGrayed); break; }
    case MenuItemType::CrtRunAhead: { AddSwitch(item, mConf.GetGlobalRunAhead(), defaultGrayed); break; }
    case MenuItemType::CrtScreenCalibration: { AddAction(item, _("CALIBRATE"), true, defaultGrayed); break; }
    case MenuItemType::Case: { AddList<String>(item, mDataProvider.GetCasesEntries(), Case::CurrentCase().ShortName(), String::Empty, false); break; }
    case MenuItemType::UserScript:
    {
      for(const ScriptDescriptor& script : mProvider.LoadUserScripts())
      {
        String name = GameFilesUtils::RemoveParenthesis(script.mPath.FilenameWithoutExtension());
        if ((script.mAttribute & (ScriptAttributes::Synced | ScriptAttributes::ReportProgress)) != 0) name.Append(' ');
        if (hasFlag(script.mAttribute, ScriptAttributes::Synced)) name.AppendUTF8(0xF1AF);
        if (hasFlag(script.mAttribute, ScriptAttributes::ReportProgress)) name.AppendUTF8(0xF1AE);
        AddAction(item, _("RUN"), true, defaultGrayed)->ReplaceParameters(name, true).MergeContext(InheritableContext(&script));
      }
      break;
    }
    case MenuItemType::SoundSystemVolumem: { AddTask(new MenuTaskSynchronizeVolumes(*AddSlider(item, 0.f, 100.f, 1.f, 80.f, (float)AudioController::Instance().GetVolume(), "%", defaultGrayed), MenuTaskSynchronizeVolumes::VolumeType::System)); break; }
    case MenuItemType::SoundMusicVolume: { AddTask(new MenuTaskSynchronizeVolumes(*AddSlider(item, 0.f, 100.f, 1.f, 80.f, (float)AudioController::Instance().GetMusicVolume(), "%", defaultGrayed), MenuTaskSynchronizeVolumes::VolumeType::Music)); break; }
    case MenuItemType::SoundAudioMode: { AddList<AudioMode>(item, mDataProvider.GetAudioModeEntries(), mConf.GetAudioMode(), AudioMode::MusicsXorVideosSound, defaultGrayed); break; }
    case MenuItemType::SoundAudioOutput: { AddTask(new MenuTaskRefreshAudioOutputs(*AddList<String>(item, mDataProvider.GetAudioOutputEntries(), AudioController::Instance().GetActivePlaybackName(), String::Empty, defaultGrayed))); break; }
    case MenuItemType::SoundBTPairing: { AddAction(item, _("CONNECT"), true, defaultGrayed); break; }
    case MenuItemType::FlattenFolders: { AddSwitch(item, mConf.GetSystemFlatFolders(*Context().System()), defaultGrayed); break; }
    case MenuItemType::FavoriteFirst: { AddSwitch(item, mConf.GetFavoritesFirst(), defaultGrayed); break; }
    case MenuItemType::DownloadSystemGames: { AddAction(item, _("DOWNLOAD"), true, defaultGrayed); break; }
    case MenuItemType::JumpToLetter: { AddList<String::Unicode>(item, mDataProvider.GetLetterEntries(Context().System()), String::UpperUnicode(Context().Game()->Name().ReadFirstUTF8()), 0, defaultGrayed); break; }
    case MenuItemType::SearchInSystem: { AddAction(item, _("SEARCH"), true, defaultGrayed); break; }
    case MenuItemType::SortGames: { AddList<FileSorts::Sorts>(item, mDataProvider.GetSortEntries(*Context().System()), mConf.GetSystemSort(*Context().System()), FileSorts::Sorts::FileNameAscending, defaultGrayed); break; }
    case MenuItemType::DecorateGames: { AddMultiList<RecalboxConf::GamelistDecoration>(item, mDataProvider.GetDecorationEntries(*Context().System()), defaultGrayed); break; }
    case MenuItemType::HighlightRegion: { AddList<Regions::GameRegions>(item, mDataProvider.GetRegionEntries(*Context().System()), Regions::Clamp(mConf.GetSystemRegionFilter(*Context().System())), Regions::GameRegions::Unknown, defaultGrayed); break; }
    case MenuItemType::RunSaveStates: { AddAction(item, _("SELECT"), true, defaultGrayed); break; }
    case MenuItemType::SetGameToBootOn: { AddSwitch(item, Context().Game()->RomPath().ToString() == mConf.GetAutorunGamePath(), defaultGrayed); break; }
    case MenuItemType::DeleteGame: { AddAction(item, _("DELETE").Append(" \uf1ca "), true, defaultGrayed); break; }
    case MenuItemType::MetadataSetEmulator: { String emulatorAndCore; AddList<String>(item, mDataProvider.GetEmulatorEntries(*Context().Game(), emulatorAndCore), emulatorAndCore, emulatorAndCore, defaultGrayed); break; }
    case MenuItemType::MetadataSoftpatching: { AddList<Path>(item, mDataProvider.GetPatchEntries(*Context().Game()), Context().Game()->Metadata().LastPatch(), Path("original"), defaultGrayed); break; }
    case MenuItemType::MetadataRatio: { AddList<String>(item, mDataProvider.GetRatioEntries(), Context().Game()->Metadata().Ratio(), "auto", defaultGrayed); break; }
    case MenuItemType::MetadataName: { AddEditor(item, Context().Game()->Metadata().Name(), false, defaultGrayed); break; }
    case MenuItemType::MetadataRating: { AddRating(item, Context().Game()->Metadata().Rating(), defaultGrayed); break; }
    case MenuItemType::MetadataGenre: { AddList<GameGenres>(item, mDataProvider.GetSingleGenreEntries(), Context().Game()->Metadata().GenreId(), GameGenres::None, defaultGrayed); break; }
    case MenuItemType::MetadataDescription: { AddEditor(item, Context().Game()->Metadata().Description(), false, defaultGrayed); break; }
    case MenuItemType::MetadataFavorite: { AddSwitch(item, Context().Game()->Metadata().Favorite(), defaultGrayed); break; }
    case MenuItemType::MetadataHidden: { AddSwitch(item, Context().Game()->Metadata().Hidden(), defaultGrayed); break; }
    case MenuItemType::MetadataAdult: { AddSwitch(item, Context().Game()->Metadata().Adult(), defaultGrayed); break; }
    case MenuItemType::MetadataRotation: { AddSwitch(item, Context().Game()->Metadata().Rotation() != RotationType::None, defaultGrayed); break; }
    case MenuItemType::MetadataScrape: { AddAction(item, _("RUN"), true, defaultGrayed); break; }
    case MenuItemType::MetastatPlaytime:
    {
      int seconds = Context().Game()->Metadata().TotalPlayTime();
      if (int hours = seconds / 3600; hours > 0) AddText(item, _N("%i HOUR", "%i HOURS", hours).Replace("%i", String(hours)));
      else if (int minutes = seconds / 60; minutes > 0) AddText(item, _N("%i MINUTE", "%i MINUTES", minutes).Replace("%i", String(minutes)));
      else AddText(item, _N("%i SECOND", "%i SECONDS", seconds).Replace("%i", String(seconds)));
      break;
    }
    case MenuItemType::MetastatPlaycount: { int count = Context().Game()->Metadata().PlayCount(); AddText(item, _N("%i TIME", "%i TIMES", count).Replace("%i", String(count)), defaultGrayed); break; }
    case MenuItemType::MetastatLastplayed:
    {
      String text;
      TimeSpan diff = DateTime() - Context().Game()->Metadata().LastPlayed();
      if (diff.IsNegative() || diff.TotalDays() > 3560) text = _("never");
      else if (diff.TotalSeconds() <  2) text = _("just now");
      else if (diff.TotalSeconds() < 60) text = _N("%i sec ago", "%i secs ago", diff.TotalSeconds()).Replace("%i", String((int)diff.TotalSeconds()));
      else if (diff.TotalMinutes() < 60) text = _N("%i min ago", "%i mins ago", diff.TotalMinutes()).Replace("%i", String((int)diff.TotalMinutes()));
      else if (diff.TotalHours()   < 24) text = _N("%i hour ago", "%i hours ago", diff.TotalHours()).Replace("%i", String((int)diff.TotalHours()));
      else text =  _N("%i day ago", "%i days ago", diff.TotalDays()).Replace("%i", String((int)diff.TotalDays()));
      AddText(item, text, defaultGrayed);
      break;
    }
    case MenuItemType::DeleteGameFiles: { int count = 1 + GameFilesUtils::GetGameSubFiles(*Context().Game()).size(); AddText(item, (_F(_N("{0} file", "{0} files", count)) / count).ToString(), defaultGrayed); break; }
    case MenuItemType::DeleteGameMedia: { int count = GameFilesUtils::GetMediaFiles(*Context().Game()).size(); AddText(item, (_F(_N("{0} file", "{0} files", count)) / count).ToString(), defaultGrayed); break; }
    case MenuItemType::DeleteFileExtra: { int count = GameFilesUtils::GetGameExtraFiles(*Context().Game()).size(); AddText(item, (_F(_N("{0} file", "{0} files", count)) / count).ToString(), defaultGrayed); break; }
    case MenuItemType::DeleteFileSaves: { int count = GameFilesUtils::GetGameSaveFiles(*Context().Game()).size(); AddText(item, (_F(_N("{0} file", "{0} files", count)) / count).ToString(), defaultGrayed); break; }
    case MenuItemType::DeleteSelectGameFiles: { AddMultiList<Path>(item, mDataProvider.GetGameFilesToDelete(*Context().Game()), defaultGrayed); break; }
    case MenuItemType::DeleteSelectGameMedia: { AddMultiList<Path>(item, mDataProvider.GetMediaFilesToDelete(*Context().Game()), defaultGrayed); break; }
    case MenuItemType::DeleteSelectFileExtra: { AddMultiList<Path>(item, mDataProvider.GetExtraFilesToDelete(*Context().Game()), defaultGrayed); break; }
    case MenuItemType::DeleteSelectFileSaves: { AddMultiList<Path>(item, mDataProvider.GetSaveFilesToDelete(*Context().Game()), defaultGrayed); break; }
    case MenuItemType::DeleteGo: { AddAction(item, "DELETE", true, defaultGrayed); break; }
    case MenuItemType::DeleteCancel: { AddAction(item, "CANCEL", false, defaultGrayed); break; }
    case MenuItemType::EnableFavorites: { AddSwitch(item, mConf.GetEnableEditFavorites(), defaultGrayed); break; }
    case MenuItemType::SoftpatchingRunOriginal: { AddAction(item, _("RUN"), true, defaultGrayed); break; }
    case MenuItemType::SoftpatchingRunPatched: { AddAction(item, _("RUN"), true, defaultGrayed); break; }
    case MenuItemType::SoftpatchingSelectPatch: { AddList<Path>(item, mDataProvider.GetPatchesEntries(*Context().Game()), Context().Game()->Metadata().LastPatch(), Path::Empty, defaultGrayed); break; }
    case MenuItemType::NetplayPassword:
    {
      for(int i = 0; i < DefaultPasswords::sPasswordCount; i++)
        AddEditor(item, ({ String pwd = mConf.GetNetplayPasswords(i); pwd.empty() ? DefaultPasswords::sDefaultPassword[i] : pwd; }), false, defaultGrayed, true)->MergeContext(InheritableContext(i));
      break;
    }
    case MenuItemType::NetplayStart: { AddAction(item, "START", true, defaultGrayed); break; }
    case MenuItemType::NetplaySelectPlayerPassword: { AddList<int>(item, mDataProvider.GetNetplayPasswords(), mConf.GetNetplayPasswordLastForPlayer(), -1, defaultGrayed); break; }
    case MenuItemType::NetplaySelectViewerPassword: { AddList<int>(item, mDataProvider.GetNetplayPasswords(), mConf.GetNetplayPasswordLastForViewer(), -1, defaultGrayed); break; }
    case MenuItemType::NetplayCancel: { AddAction(item, "CANCEL", false, defaultGrayed); break; }
    case MenuItemType::NetplayJoinAsPlayer: { AddAction(item, "JOIN", true, defaultGrayed); break; }
    case MenuItemType::NetplayJoinAsViewer: { AddAction(item, "WATCH", true, defaultGrayed); break; }
    case MenuItemType::NetplayUsePassword: { AddList<int>(item, mDataProvider.GetNetplayPasswords(), mConf.GetNetplayPasswordClient(), -1, defaultGrayed); break; }
    case MenuItemType::_Error_:
    default: { LOG(LogFatal) << "[MenuProvider] Cannot create menu item: " << MenuConverters::ItemToString(item.Type()); break; }
  }
}

/*
 * Following method store & check menu items
 * Most of the menu items need to be singletons and are so checked against duplication
 * However some contextualized items **are** duplicated and reconized only by their context
 * Such items do not need to be checked and are not stored in type => item map so that they cannot be retrieved
 * later using their identifier.
 */

void MenuBuilder::CheckDuplicate(const ItemDefinition& item, bool acceptDuplicates)
{
  if (!acceptDuplicates)
   if (mItemsTypes.contains(item.Type()))
      { LOG(LogFatal) << "[MenuBuildert] Item of type " << (int)item.Type() << " captionned '" << item.RawCaption() << "' is duplicated in current menu!"; }
}

ItemText* MenuBuilder::AddText(const ItemDefinition& item, const String& text, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemText* object = Menu::AddText(item.Caption(this), text, item.Help());
  object->MergeContext(Context()).ReplaceParameters();
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemBar* MenuBuilder::AddBar(const ItemDefinition& item, const String& text, float ratio, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemBar* object = Menu::AddBar(item.Caption(this), text, ratio, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context()).ReplaceParameters();
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemAction* MenuBuilder::AddAction(const ItemDefinition& item, const String& action, bool positive, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemAction* object = Menu::AddAction(item.Icon(), item.Caption(this), _S(action), (int)item.Type(), positive, &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context()).ReplaceParameters();
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemSwitch* MenuBuilder::AddSwitch(const ItemDefinition& item, bool state, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemSwitch* object = Menu::AddSwitch(item.Caption(this), state, (int)item.Type(), &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context()).ReplaceParameters();
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemEditable* MenuBuilder::AddEditor(const ItemDefinition& item, const String& editTitle, const String& text, bool masked, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemEditable* object = AddEditable(editTitle, item.Caption(this), text, (int)item.Type(), &mProvider, item.Help(), masked, grayed, item.UnselectableHelp());
  object->MergeContext(Context()).ReplaceParameters();
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemEditable* MenuBuilder::AddEditor(const ItemDefinition& item, const String& text, bool masked, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemEditable* object = AddEditable(item.Caption(this), text, (int)item.Type(), &mProvider, item.Help(), masked, grayed, item.UnselectableHelp());
  object->MergeContext(Context()).ReplaceParameters();
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemSlider*
MenuBuilder::AddSlider(const ItemDefinition& item, float min, float max, float inc, float value, float defaultvalue,
                       const String& suffix, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemSlider* object = Menu::AddSlider(item.Caption(this), min, max, inc, value, defaultvalue, suffix, (int)item.Type(), &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context()).ReplaceParameters();
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemSubMenu* MenuBuilder::AddSubMenu(const ItemDefinition& item, bool grayed)
{
  // Menu item are duplicable by default, and they are not stored.
  ItemSubMenu* object = Menu::AddSubMenu(item.Caption(this), (int)item.MenuType(), &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context()).ReplaceParameters();
  return object;
}

ItemRating* MenuBuilder::AddRating(const ItemDefinition& item, float rating, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemRating* object = Menu::AddRating(item.Caption(this), rating, (int)item.Type(), &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context()).ReplaceParameters();
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

String MenuBuilder::Extend(const InheritableContext& context, const String& rawText)
{
  static String systemName("{SYSTEM.NAME}");
  static String gameName("{GAME.NAME}");
  static String index("{INDEX}");
  static String unknownSystem("<No system available>");
  static String unknownGame("<No game available>");
  return String(rawText).Replace(systemName, context.HasSystem() ? context.System()->Descriptor().FullName() : unknownSystem)
                         .Replace(gameName, context.HasGame() ? context.Game()->Name() : unknownGame)
                         .Replace(index, String(context.Index()));
}

template<class T>
ItemSelector<T>* MenuBuilder::AddList(const ItemDefinition& item, const SelectorEntry<T>::List& values, const T& value, const T& defaultValue, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemSelector<T>* object = Menu::AddList<T>(item.Caption(this), (int)item.Type(), &mProvider, values, value, defaultValue, item.Help(), grayed, item.UnselectableHelp());
  if (object != nullptr)
  {
    object->MergeContext(Context()).ReplaceParameters();
    mItemsLinks[object] = &item;
    if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  }
  return object;
}

template<class T>
ItemSelector<T>* MenuBuilder::AddMultiList(const ItemDefinition& item, const SelectorEntry<T>::List& values, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemSelector<T>* object = Menu::AddMultiList<T>(item.Caption(this), (int)item.Type(), &mProvider, values, item.Help(), grayed, item.UnselectableHelp());
  if (object != nullptr)
  {
    object->MergeContext(Context()).ReplaceParameters();
    mItemsLinks[object] = &item;
    if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  }
  return object;
}

