//
// Created by bkg2k on 25/11/24.
//

#include "MenuModalFactoryReset.h"
#include <recalbox/RecalboxSystem.h>
#include <recalbox/BootConf.h>

bool MenuModalFactoryReset::TaskExecute(const bool& parameter)
{
  (void)parameter;
  String::List deleteMe
    ({
       "/recalbox/share/system",                       // Recalbox & emulator configurations
       "/overlay/upper/*",                             // System overlay
       "/overlay/.configs/*",                          // System configurations
       "/overlay/upper.old",                           // System overlay backup
       "/overlay/.config",                             // Old system configurations
       "/boot/recalbox-backup.conf",                   // Recalbox configuration backup
       "/boot/crt/recalbox-crt-config.txt",            // CRT Configuration
       "/boot/crt/recalbox-crt-options.cfg",           // CRT Configuration
       "/boot/crt/recalbox-crt-options.cfg.backup",    // CRT Configuration
       "/boot/crt/rrgbd.hdmi",                         // CRT Configuration
       "/boot/crt/rrgbd.crt",                          // CRT Configuration
       "/boot/crt/.currentvideoconfig",                // CRT Configuration
       "/boot/crt/.stamprrgbdual",                     // CRT Configuration
       "/boot/crt/.stamprrgbdual31khz",                // CRT Configuration
       "/boot/crt/.stamphdmiforced",                   // CRT Configuration
     });

  // Make boot partition writable
  if (!RecalboxSystem::MakeBootReadWrite())
  { LOG(LogError) << "[ResetFactory] Error making boot r/w"; }

  // Delete all required folder/files
  for(const String& path : deleteMe)
    if (system(String("rm -rf ").Append(path).data()) != 0)
    { LOG(LogError) << "[ResetFactory] Error removing folder " << path; }
    else Thread::Sleep(200);

  IniFile& recalboxBoot(BootConf::Instance());
  // Reset rotation
  recalboxBoot.SetString("screen.rotation", "0");
  // Special case for rpizero plus GPiCase2W, that cannot be detected. Can be removed after the rpizero image is frozen
  if(Board::Instance().GetBoardType() == BoardType::Pi0 && recalboxBoot.AsString("case").starts_with("GPi2W"))
    recalboxBoot.SetString("case", "GPi2W");
  else
    recalboxBoot.SetString("case", "");

  recalboxBoot.Save();

  // Reset!
  if (system("shutdown -r now") != 0)
  { LOG(LogError) << "[ResetFactory] Error rebooting system"; }

  return true;
}
