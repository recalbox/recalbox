//
// Created by bkg2k on 07/02/25.
//

#include "MenuDataProvider.h"
#include "netplay/DefaultPasswords.h"
#include "games/GameFilesUtils.h"
#include "EmulationStation.h"
#include "systems/arcade/ArcadeVirtualSystems.h"
#include "ResolutionAdapter.h"
#include "LibretroRatio.h"
#include "scraping/ScraperFactory.h"
#include <guis/menus/MenuTools.h>
#include <audio/AudioController.h>
#include <views/ViewController.h>
#include <algorithm>

SelectorEntry<StorageDevices::Device>::List MenuDataProvider::GetStorageEntries(int& currentIndex)
{
  SelectorEntry<StorageDevices::Device>::List list;
  int index = 0;
  for(const StorageDevices::Device& device : StorageDevices().GetShareDevices())
  {
    String display = device.DisplayName;
    if (device.Size != 0)
      display.Append(" (").Append(_("Free"))
             .Append(' ').Append(device.HumanFree())
             .Append('/').Append(device.HumanSize()).Append(')');
    list.push_back({ display, device, device.Current });
    if (device.Current) currentIndex = index;
    index++;
  }
  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetCultureEntries()
{
  return SelectorEntry<String>::List
    ({
       { "اللغة العربية" , "ar_YE" },
       { "CATALÀ"        , "ca_ES" },
       { "čeština"       , "cs_CZ" },
       { "DEUTSCH"       , "de_DE" },
       { "DEUTSCH (CH)"  , "de_CH" },
       { "ελληνικά"      , "el_GR" },
       { "ENGLISH"       , "en_US" },
       { "ENGLISH (UK)"  , "en_GB" },
       { "ESPAÑOL"       , "es_ES" },
       { "EUSKARA"       , "eu_ES" },
       { "PERSIAN"       , "fa_IR" },
       { "FRANÇAIS"      , "fr_FR" },
       { "GALICIAN"      , "gl_ES" },
       { "MAGYAR"        , "hu_HU" },
       { "ITALIANO"      , "it_IT" },
       { "日本語"         , "ja_JP" },
       { "한국어"         , "ko_KR" },
       { "Lëtzebuergesch", "lb_LU" },
       { "latviešu"      , "lv_LV" },
       { "BOKMAL"        , "nb_NO" },
       { "NEDERLANDS"    , "nl_NL" },
       { "NORSK"         , "nn_NO" },
       { "POLSKI"        , "pl_PL" },
       { "PORTUGUES"     , "pt_BR" },
       { "ROMANIAN"      , "ro_RO" },
       { "Русский"       , "ru_RU" },
       { "SLOVAK"        , "sk_SK" },
       { "SVENSKA"       , "sv_SE" },
       { "TÜRKÇE"        , "tr_TR" },
       { "UKRAINIAN"     , "uk_UA" },
       { "简体中文"       , "zh_CN" },
       { "正體中文"       , "zh_TW" },
     });
}

SelectorEntry<String>::List MenuDataProvider::GetKeyboardEntries()
{
  return SelectorEntry<String>::List
    ({
       { "DE (German QWERTZ)"  , "de" },
       { "DK (Denmark QWERTY)" , "dk" },
       { "ES (Spanish QWERTY)" , "es" },
       { "FR (French AZERTY)"  , "fr" },
       { "IT (Italian QWERTY)" , "it" },
       { "US (Standard QWERTY)", "us" },
     });
}

SelectorEntry<String>::List MenuDataProvider::GetTimeZones()
{
  // Get timezones
  static Path timezoneRoot("/usr/share/zoneinfo/posix");
  Path::PathList timezones = timezoneRoot.RecurseDirectoryContent(false);
  // Remove base path
  bool dummy;
  for(Path& tz : timezones) tz = tz.MakeRelative(timezoneRoot, dummy);
  // Sort
  std::sort(timezones.begin(), timezones.end());

  // Build final structure
  SelectorEntry<String>::List result;
  for(Path& tz : timezones)
    result.push_back({ String(tz.ToString()).Replace('/', " - ", 3), tz.ToString() });

  return result;
}

SelectorEntry<String>::List MenuDataProvider::GetKodiResolutionsEntries()
{
  SelectorEntry<String>::List result;

  result.push_back({ _("NATIVE"), "default" });
  result.push_back({ _("USE GLOBAL"), "" });
  for(const ResolutionAdapter::Resolution& resolution : mResolutionAdapter.Resolutions(false))
  {
    String reso = resolution.ToRawString();
    result.push_back({ resolution.ToString(), reso });
  }
  return result;
}

SelectorEntry<ScraperNameOptions>::List MenuDataProvider::GetScraperNameOptionsEntries()
{
  SelectorEntry<ScraperNameOptions>::List list;
  list.push_back({ _("Scraper results"), ScraperNameOptions::GetFromScraper });
  list.push_back({ _("Raw filename"), ScraperNameOptions::GetFromFilename });
  list.push_back({ _("Undecorated filename"), ScraperNameOptions::GetFromFilenameUndecorated });
  return list;
}

SelectorEntry<ScrapingMethod>::List MenuDataProvider::GetScrapingMethods()
{
  SelectorEntry<ScrapingMethod>::List list;
  list.push_back({ _("All Games"), ScrapingMethod::All });
  list.push_back({ _("Only missing image"), ScrapingMethod::AllIfNothingExists });
  return list;
}

SelectorEntry<ScraperType>::List MenuDataProvider::GetScrapersEntries()
{
  SelectorEntry<ScraperType>::List list;
  for(const auto& kv : ScraperFactory::GetScraperList())
    list.push_back({ kv.second, kv.first });
  return list;
}

SelectorEntry<SystemData*>::List MenuDataProvider::GetScrapableSystemsEntries()
{
  SelectorEntry<SystemData*>::List list;
  for(SystemData* system : mSystemManager.VisibleSystemList())
  {
    if (!system->IsScreenshots()) // Only screenshot doesn't need to be scraped
      if (system->HasScrapableGame())
        list.push_back({ system->FullName(), system, ScraperFactory::Instance().HasSystem(system) });
  }
  return list;
}

SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List MenuDataProvider::GetScraperImagesEntries()
{
  SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List list;
  list.push_back({ _("In-game screenshot"), ScreenScraperEnums::ScreenScraperImageType::ScreenshotIngame });
  list.push_back({ _("Title screenshot"),   ScreenScraperEnums::ScreenScraperImageType::ScreenshotTitle });
  list.push_back({ _("Clear logo"),         ScreenScraperEnums::ScreenScraperImageType::Wheel });
  list.push_back({ _("Marquee"),            ScreenScraperEnums::ScreenScraperImageType::Marquee });
  list.push_back({ _("box2D"),              ScreenScraperEnums::ScreenScraperImageType::Box2d });
  list.push_back({ _("box3d"),              ScreenScraperEnums::ScreenScraperImageType::Box3d });
  list.push_back({ _("ScreenScraper Mix V1"),ScreenScraperEnums::ScreenScraperImageType::MixV1 });
  list.push_back({ _("ScreenScraper Mix V2"), ScreenScraperEnums::ScreenScraperImageType::MixV2 });
  return list;
}

SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List MenuDataProvider::GetScraperThumbnailsEntries()
{
  SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List list;
  list.push_back({ _("No thumbnail"),       ScreenScraperEnums::ScreenScraperImageType::None });
  list.push_back({ _("In-game screenshot"), ScreenScraperEnums::ScreenScraperImageType::ScreenshotIngame });
  list.push_back({ _("Title screenshot"),   ScreenScraperEnums::ScreenScraperImageType::ScreenshotTitle });
  list.push_back({ _("Clear logo"),         ScreenScraperEnums::ScreenScraperImageType::Wheel });
  list.push_back({ _("Marquee"),            ScreenScraperEnums::ScreenScraperImageType::Marquee });
  list.push_back({ _("box2D"),              ScreenScraperEnums::ScreenScraperImageType::Box2d });
  list.push_back({ _("box3d"),              ScreenScraperEnums::ScreenScraperImageType::Box3d });
  list.push_back({ _("ScreenScraper Mix V1"), ScreenScraperEnums::ScreenScraperImageType::MixV1 });
  list.push_back({ _("ScreenScraper Mix V2"), ScreenScraperEnums::ScreenScraperImageType::MixV2 });
  return list;
}

SelectorEntry<ScreenScraperEnums::ScreenScraperVideoType>::List MenuDataProvider::GetScraperVideosEntries()
{
  SelectorEntry<ScreenScraperEnums::ScreenScraperVideoType>::List list;
  list.push_back({ _("No video"), ScreenScraperEnums::ScreenScraperVideoType::None });
  list.push_back({ String(_("Optimized video")).Append(" (").Append(_("RECOMMENDED")).Append(')'), ScreenScraperEnums::ScreenScraperVideoType::Optimized });
  list.push_back({ _("Original video"), ScreenScraperEnums::ScreenScraperVideoType::Raw});
  return list;
}

SelectorEntry<ScreenScraperEnums::ScreenScraperRegionPriority>::List MenuDataProvider::GetScraperRegionOptionsEntries()
{
  SelectorEntry<ScreenScraperEnums::ScreenScraperRegionPriority>::List list;
  list.push_back({_("DETECTED REGION"), ScreenScraperEnums::ScreenScraperRegionPriority::DetectedRegion });
  list.push_back({_("FAVORITE REGION"), ScreenScraperEnums::ScreenScraperRegionPriority::FavoriteRegion });
  return list;
}

SelectorEntry<Regions::GameRegions>::List MenuDataProvider::GetScraperRegionsEntries()
{
  SelectorEntry<Regions::GameRegions>::List list;
  for(Regions::GameRegions region : Regions::AvailableRegions())
  {
    if(Regions::GameRegions::Unknown == region) continue;
    list.push_back({ Regions::RegionFullName(region), region });
  }
  return list;
}

SelectorEntry<Languages>::List MenuDataProvider::GetScraperLanguagesEntries()
{
  SelectorEntry<Languages>::List list;
  for(Languages language : LanguagesTools::AvailableLanguages())
  {
    if (language == Languages::Unknown) continue;
    list.push_back({ LanguagesTools::LanguagesFullName(language), language });
  }
  return list;
}

SelectorEntry<RecalboxConf::SoftPatching>::List& MenuDataProvider::GetSoftpatchingEntries()
{
  static SelectorEntry<RecalboxConf::SoftPatching>::List list
    {
      { _("AUTO"), RecalboxConf::SoftPatching::Auto },
      { _("LAUNCH LAST"), RecalboxConf::SoftPatching::LaunchLast },
      { _("SELECT"), RecalboxConf::SoftPatching::Select },
      { _("DISABLE"), RecalboxConf::SoftPatching::Disable },
    };
  return list;
}

SelectorEntry<String>::List& MenuDataProvider::GetSuperGameBoyEntries()
{
  static SelectorEntry<String>::List list
    {
      { _("GAME BOY"), "gb" },
      { _("SUPER GAME BOY"), "sgb" },
      { _("ASK AT LAUNCH"), "ask" },
    };
  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetRatioEntries()
{
  SelectorEntry<String>::List list;
  for (const auto& ratio : LibretroRatio::GetRatio())
    list.push_back({ ratio.first, ratio.second });

  return list;
}

SelectorEntry<String>::List& MenuDataProvider::GetShaderPresetsEntries()
{
  static SelectorEntry<String>::List list
    {
      { _("NONE"), "none" },
      { _("CRT CURVED"), "crtcurved" },
      { _("SCANLINES"), "scanlines" },
      { _("RETRO"), "retro" },
    };
  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetShadersEntries()
{
  SelectorEntry<String>::List list;
  MenuTools::ShaderList shaderList = MenuTools::ListShaders();
  list.push_back({ _("NONE"), "" });
  for (const MenuTools::Shader& shader : shaderList)
    list.push_back({ shader.Displayable, shader.ShaderPath.ToString() });
  return list;
}

SelectorEntry<RecalboxConf::Relay>::List& MenuDataProvider::GetMitmEntries()
{
  static SelectorEntry<RecalboxConf::Relay>::List list
    {
      { _("NONE"), RecalboxConf::Relay::None },
      { _("NEW YORK"), RecalboxConf::Relay::NewYork },
      { _("MADRID"), RecalboxConf::Relay::Madrid },
      { _("MONTREAL"), RecalboxConf::Relay::Montreal },
      { _("SAOPAULO"), RecalboxConf::Relay::Saopaulo },
    };
  return list;
}

SelectorEntry<RecalboxConf::UpdateType>::List MenuDataProvider::GetUpdateTypeEntries()
{
  SelectorEntry<RecalboxConf::UpdateType>::List list;

  list.push_back({ "stable", RecalboxConf::UpdateType::Stable });
  list.push_back({ "patron", RecalboxConf::UpdateType::Patron });
  #if defined(BETA) || defined(DEBUG)
  list.push_back({ "alpha" , RecalboxConf::UpdateType::Alpha });
  list.push_back({ "jamma" , RecalboxConf::UpdateType::Jamma });
  #endif

  return list;
}

SelectorEntry<RecalboxConf::Screensaver>::List MenuDataProvider::GetTypeEntries()
{
  SelectorEntry<RecalboxConf::Screensaver>::List list;

  RecalboxConf::Screensaver type = mConf.GetScreenSaverType();
  if (Board::Instance().HasSuspendResume())
    list.push_back({ _("suspend"), RecalboxConf::Screensaver::Suspend, type == RecalboxConf::Screensaver::Suspend });
  list.push_back({ _("dim"), RecalboxConf::Screensaver::Dim, type == RecalboxConf::Screensaver::Dim });
  list.push_back({ _("black"), RecalboxConf::Screensaver::Black, type == RecalboxConf::Screensaver::Black });
  list.push_back({ _("demo"), RecalboxConf::Screensaver::Demo, type == RecalboxConf::Screensaver::Demo });
  list.push_back({ _("gameclip"), RecalboxConf::Screensaver::Gameclip, type == RecalboxConf::Screensaver::Gameclip });

  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetSystemListAsString()
{
  SelectorEntry<String>::List list;

  for (const SystemData* system : mSystemManager.AllSystems())
    if (system->HasGame() && !system->Descriptor().IsPort())
      list.push_back({ system->FullName(), system->Name(), mConf.IsInScreenSaverSystemList(system->Name()) });

  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetThemeTransitionEntries()
{
  SelectorEntry<String>::List list;

  String originalTransition = mConf.GetThemeTransition();
  list.push_back({ _("FADE"), "fade", originalTransition == "fade" });
  list.push_back({ _("SLIDE"), "slide", originalTransition == "slide" });
  list.push_back({ _("INSTANT"), "instant", originalTransition == "instant" });

  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetThemeRegionEntries()
{
  SelectorEntry<String>::List list;

  String region = mConf.GetThemeRegion();
  list.push_back({ _("Europe"), "eu", region == "eu" });
  list.push_back({ _("USA"), "us", region == "us" });
  list.push_back({ _("Japan"), "jp", region == "jp" });

  return list;
}

ThemeSpec MenuDataProvider::GetCurrentTheme()
{
  ThemeManager::ThemeList themelist = ThemeManager::AvailableThemes();
  if (themelist.contains(mConf.GetThemeFolder())) return { mConf.GetThemeFolder(), themelist[mConf.GetThemeFolder()] };
  return { String::Empty, Path::Empty };
}

ThemeSpec MenuDataProvider::GetDefaultTheme()
{
  ThemeManager::ThemeList themelist = ThemeManager::AvailableThemes();
  if (themelist.contains(ThemeManager::sDefaultThemeFolder)) return { ThemeManager::sDefaultThemeFolder, themelist[ThemeManager::sDefaultThemeFolder] };
  return { themelist.begin()->first, themelist.begin()->second };
}

SelectorEntry<SystemSorting>::List& MenuDataProvider::GetSystemSortingEntries()
{
  static SelectorEntry<SystemSorting>::List list
    {
      { _("DEFAULT")                                    , SystemSorting::Default                                    },
      { _("Name")                                       , SystemSorting::Name                                       },
      { _("RELEASE DATE")                               , SystemSorting::ReleaseDate                                },
      { _("TYPE, THEN NAME")                            , SystemSorting::SystemTypeThenName                         },
      { _("TYPE, THEN RELEASE DATE")                    , SystemSorting::SystemTypeThenReleaseDate                  },
      { _("MANUFACTURER, THEN NAME")                    , SystemSorting::ManufacturerThenName                       },
      { _("MANUFACTURER, THEN RELEASE DATE")            , SystemSorting::ManufacturerThenReleaseData                },
      { _("TYPE, THEN MANUFACTURER, THEN NAME")         , SystemSorting::SystemTypeThenManufacturerThenName         },
      { _("TYPE, THEN MANUFACTURER, THEN RELEASE DATE") , SystemSorting::SystemTypeThenManufacturerThenReleasdeDate },
    };
  return list;
}

SelectorEntry<SystemData*>::List MenuDataProvider::GetHidableSystems()
{
  SelectorEntry<SystemData*>::List result;
  for(SystemData* system : mSystemManager.AllSystems())
    if (!system->IsPorts() && !system->IsVirtual())
      result.push_back({ system->FullName(), system, mConf.GetSystemIgnore(*system) });
  return result;
}

SelectorEntry<ThemeSpec>::List MenuDataProvider::GetThemeEntries(WindowManager& window)
{
  // Get theme list
  ThemeManager::ThemeList themelist = ThemeManager::AvailableThemes();

  String currentVersionString = PROGRAM_VERSION_STRING;
  int cut = (int)currentVersionString.find_first_not_of("0123456789.");
  if (cut >= 0) currentVersionString.Delete(cut, INT32_MAX);

  // Sort names
  //! Theme spec list
  std::vector<ThemeSpec> sortedNames;
  for (const auto& kv : themelist) sortedNames.push_back({ kv.first, kv.second });
  std::sort(sortedNames.begin(), sortedNames.end(), [](const ThemeSpec& a, const ThemeSpec& b) { return a.Name.ToLowerCase() < b.Name.ToLowerCase(); });

  SelectorEntry<ThemeSpec>::List list;
  for (const ThemeSpec& theme : sortedNames)
  {
    String displayableName;
    bool compatible = ThemeManager::Instance().AnalyseAndSwitch(window, theme.FolderPath, displayableName, false);

    // (v) - compatibility match else /!\ sign - compatibility mismatch
    displayableName.Insert(0, compatible ? "\uF1C0 " : "\uF1CA ");

    list.push_back({ displayableName, theme });
  }

  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetManufacturersVirtualEntries()
{
  SelectorEntry<String>::List result;
  const RecalboxConf& conf = RecalboxConf::Instance();

  for(const std::pair<String, String>& rawIdentifier : ArcadeVirtualSystems::GetVirtualArcadeSystemListExtended())
  {
    String identifier(SystemManager::sArcadeManufacturerPrefix);
    identifier.Append(rawIdentifier.first).Replace('\\', '-');
    result.push_back({ rawIdentifier.second, identifier, conf.IsInCollectionArcadeManufacturers(identifier) });
  }
  return result;
}

SelectorEntry<String>::List MenuDataProvider::GetBootSystemEntries()
{
  SelectorEntry<String>::List list;

  // For each activated system
  for (SystemData* system : mSystemManager.VisibleSystemList())
    list.push_back({ system->FullName(), system->Name() });

  return list;
}

SelectorEntry<GameGenres>::List MenuDataProvider::GetMultiGenreEntries()
{
  SelectorEntry<GameGenres>::List list;

  for(const GameGenres genre : Genres::GetOrderedList())
  {
    String longName = Genres::GetFullName(genre);
    String prefix = Genres::IsSubGenre(genre) ? "   \u21B3 " : "";
    //Path icon = Path(Genres::GetResourcePath(genre));
    list.push_back({ prefix.Append(_S(longName)), genre, mConf.IsInCollectionGenre(SystemManager::BuildGenreSystemName(genre)) });
  }
  return list;
}

SelectorEntry<GameGenres>::List MenuDataProvider::GetSingleGenreEntries()
{
  SelectorEntry<GameGenres>::List list;

  for(const GameGenres genre : Genres::GetOrderedList())
  {
    String longName = Genres::GetFullName(genre);
    String prefix = Genres::IsSubGenre(genre) ? "   \u21B3 " : "";
    list.push_back({ prefix.Append(_S(longName)), genre });
  }
  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetGlobalResolutionEntries()
{
  SelectorEntry<String>::List result;
  for(const ResolutionAdapter::Resolution& resolution : mResolutionAdapter.Resolutions(true))
  {
    String reso = resolution.ToRawString();
    result.push_back({ resolution.ToString(), reso });
  }
  return result;
}

SelectorEntry<String>::List MenuDataProvider::GetResolutionEntries()
{
  SelectorEntry<String>::List result;
  result.push_back({ _("USE GLOBAL").Append(" (").Append(MenuTools::GetResolutionText(mConf.GetGlobalVideoMode())).Append(')'), "" });
  result.push_back({ _("NATIVE").Append(" (").Append(MenuTools::GetResolutionText("default")).Append(')'), "default" });
  for(const ResolutionAdapter::Resolution& resolution : mResolutionAdapter.Resolutions(true))
  {
    String reso = resolution.ToRawString();
    result.push_back({ resolution.ToString(), reso });
  }
  return result;
}

SelectorEntry<Overclocking::Overclock>::List MenuDataProvider::GetOverclockEntries()
{
  SelectorEntry<Overclocking::Overclock>::List list;

  // Add entries
  const Overclocking::List& oc = Board::Instance().GetOverclocking().GetOverclockList();
  for(const Overclocking::Overclock& overclock : oc)
  {
    String label = String(overclock.Description()).Append(" (", 2).Append(overclock.Frequency()).Append(" Mhz)", 5).Append(overclock.Hazardous() ? " \u26a0" : "");
    list.push_back({ label, overclock, false });
  }
  std::sort(list.begin(), list.end(), [](const SelectorEntry<Overclocking::Overclock>& a, const SelectorEntry<Overclocking::Overclock>& b) { return a.mValue.Frequency() < b.mValue.Frequency(); });
  // Add none
  if (!list.empty())
    list.insert(list.begin(), { _("NONE"), Overclocking::NoOverclock(), false });

  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetEmulatorEntries(const SystemData* system, String& emulatorAndCore, String& defaultEmulatorAndCore)
{
  SelectorEntry<String>::List list;

  String currentEmulator(mConf.GetSystemEmulator(*system));
  String currentCore    (mConf.GetSystemCore(*system));
  String dummy;
  MenuTools::EmulatorAndCoreList eList =
    MenuTools::ListEmulatorAndCore(*system, dummy, dummy, currentEmulator, currentCore);
  if (!eList.empty())
    for (const MenuTools::EmulatorAndCore& emulator : eList)
    {
      list.push_back({ emulator.Displayable, emulator.Identifier });
      if (emulator.Selected) emulatorAndCore = emulator.Identifier;
      if (defaultEmulatorAndCore.empty()) defaultEmulatorAndCore = emulator.Identifier;
    }

  return list;
}

SelectorEntry<CrtAdapterType>::List MenuDataProvider::GetDacEntries()
{
  SelectorEntry<CrtAdapterType>::List list;

  if(Board::Instance().CrtBoard().GetCrtAdapter() == CrtAdapterType::RGBDual)
  {
    list.push_back( { "Recalbox RGB Dual", CrtAdapterType::RGBDual, true } );
    return list;
  }

  CrtAdapterType selectedDac = CrtConf::Instance().GetSystemCRT();

  static struct
  {
    const char* Name;
    CrtAdapterType Type;
  }
    Adapters[] =
    {
      { "Recalbox RGB Dual", CrtAdapterType::RGBDual },
      { "Recalbox RGB Jamma", CrtAdapterType::RGBJamma },
      { "VGA666", CrtAdapterType::Vga666 },
      { "RGBPi", CrtAdapterType::RGBPi },
      { "Pi2SCART", CrtAdapterType::Pi2Scart },
    };

  // Always push none
  list.push_back( { _("NONE"), CrtAdapterType::None, selectedDac == CrtAdapterType::None } );
  // Push all adapters or only one if it is automatically detected
  const ICrtInterface& crt = Board::Instance().CrtBoard();
  for(const auto& item : Adapters)
  {
    if (!crt.HasBeenAutomaticallyDetected() || crt.GetCrtAdapter() == item.Type)
      list.push_back( { item.Name, item.Type, selectedDac == item.Type } );
  }

  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetEsResolutionEntries()
{
  SelectorEntry<String>::List list;
  ICrtInterface::HorizontalFrequency currentScreen = Board::Instance().CrtBoard().GetHorizontalFrequency();

  std::string baseRez = CrtConf::Instance().GetSystemCRTResolution();
  if(baseRez == "0")
  {
    switch(currentScreen){
      case ICrtInterface::HorizontalFrequency::Auto:
      case(ICrtInterface::HorizontalFrequency::KHz15): baseRez = "240"; break;
      case(ICrtInterface::HorizontalFrequency::KHzMulti1525): baseRez = "384"; break;
      case(ICrtInterface::HorizontalFrequency::KHzTriFreq):
      case(ICrtInterface::HorizontalFrequency::KHzMulti1531):
      case(ICrtInterface::HorizontalFrequency::KHz31): baseRez = "480"; break;
    }
  }

  // Each monitor > KHz31 has 480p support
  if(currentScreen >= ICrtInterface::HorizontalFrequency::KHz31)
  {
    list.push_back({ "480p", "480", baseRez == "480" });
    if(Board::Instance().CrtBoard().Has120HzSupport())
      list.push_back({ "240p@120Hz", "240", baseRez == "240" });
  }

  // Tri and multi with 25
  if(currentScreen == ICrtInterface::HorizontalFrequency::KHzTriFreq || currentScreen == ICrtInterface::HorizontalFrequency::KHzMulti1525)
    list.push_back({ "384p", "384", baseRez == "384" });

  // All with 15khz
  if(currentScreen == ICrtInterface::HorizontalFrequency::KHzTriFreq
     || currentScreen == ICrtInterface::HorizontalFrequency::KHzMulti1525
     || currentScreen == ICrtInterface::HorizontalFrequency::KHz15
     || currentScreen == ICrtInterface::HorizontalFrequency::KHzMulti1531)
    list.push_back({ "240p", "240", baseRez == "240" });

  // All with 15khz
  if(currentScreen == ICrtInterface::HorizontalFrequency::KHz15 && Board::Instance().CrtBoard().HasInterlacedSupport())
    list.push_back({ "480i", "480", baseRez == "480" });

  return list;
}

const SelectorEntry<ICrtInterface::HorizontalFrequency>::List& MenuDataProvider::GetHorizontalOutputFrequency()
{
  static SelectorEntry<ICrtInterface::HorizontalFrequency>::List result
    {
      { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHz15), ICrtInterface::HorizontalFrequency::KHz15 },
      { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHz31), ICrtInterface::HorizontalFrequency::KHz31 },
      { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHzMulti1525), ICrtInterface::HorizontalFrequency::KHzMulti1525 },
      { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHzMulti1531), ICrtInterface::HorizontalFrequency::KHzMulti1531 },
      { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHzTriFreq), ICrtInterface::HorizontalFrequency::KHzTriFreq },
    };
  return result;
}

const SelectorEntry<String>::List& MenuDataProvider::GetSuperRezEntries()
{
  static SelectorEntry<String>::List list
    {
      { "X6 (DEFAULT)", "1920" },
      { "X8", "2560" },
      { "DYNAMIC", "1" },
      { "NATIVE", "0" },
    };
  return list;
}

const SelectorEntry<CrtScanlines>::List& MenuDataProvider::GetScanlinesEntries()
{
  static SelectorEntry<CrtScanlines>::List list
    {
      { "NONE", CrtScanlines::None },
      { "LIGHT", CrtScanlines::Light },
      { "MEDIUM", CrtScanlines::Medium },
      { "HEAVY", CrtScanlines::Heavy },
    };
  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetCasesEntries()
{
  SelectorEntry<String>::List list;
  Case currentCase = Case::CurrentCase();
  if (currentCase.Automatic()) list.push_back({currentCase.DisplayName() + " (AUTO)", currentCase.ShortName(), true});
  else {
    for(const Case& theCase : Case::SupportedManualCases()) list.push_back({theCase.DisplayName(), theCase.ShortName(), theCase.Model() == currentCase.Model()});
  }
  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetAudioOutputEntries()
{
  SelectorEntry<String>::List list;
  // Add all remaining stuff
  for(const DeviceDescriptor& playback : AudioController::Instance().GetPlaybackList())
    list.push_back({ playback.DisplayableName, playback.InternalName });
  return list;
}

const SelectorEntry<AudioMode>::List& MenuDataProvider::GetAudioModeEntries()
{
  static SelectorEntry<AudioMode>::List list
    {
      { _("Musics or videos sound"), AudioMode::MusicsXorVideosSound },
      { _("Musics and videos sound"), AudioMode::MusicsAndVideosSound },
      { _("Musics only"), AudioMode::MusicsOnly },
      { _("Videos sound only"), AudioMode::VideosSoundOnly },
      { _("No sound"), AudioMode::None },
    };
  return list;
}

SelectorEntry<String::Unicode>::List MenuDataProvider::GetLetterEntries(SystemData* system)
{
  SelectorEntry<String::Unicode>::List list;

  // Get available letters
  ISimpleGameListView* gamelist = ViewController::Instance().GetOrCreateGamelistView(system, false);
  Array<String::Unicode> letters = gamelist->GetAvailableLetters();
  if (!letters.Empty())
  {
    // Build list
    for (String::Unicode unicode : letters)
      list.push_back({ String(unicode, 1), unicode });
  }

  return list;
}

SelectorEntry<RecalboxConf::GamelistDecoration>::List MenuDataProvider::GetDecorationEntries(const SystemData& system)
{
  (void)system;
  SelectorEntry<RecalboxConf::GamelistDecoration>::List list;
  RecalboxConf::GamelistDecoration decorations = mConf.GetSystemGamelistDecoration(*ViewController::Instance().CurrentSystem());
  list.push_back({ "Region flags", RecalboxConf::GamelistDecoration::Regions, hasFlag(decorations, RecalboxConf::GamelistDecoration::Regions) });
  list.push_back({ "Players", RecalboxConf::GamelistDecoration::Players, hasFlag(decorations, RecalboxConf::GamelistDecoration::Players) });
  list.push_back({ "Genres", RecalboxConf::GamelistDecoration::Genre, hasFlag(decorations, RecalboxConf::GamelistDecoration::Genre) });
  list.push_back({ "Support numbers", RecalboxConf::GamelistDecoration::Support, hasFlag(decorations, RecalboxConf::GamelistDecoration::Support) });
  return list;
}

SelectorEntry<Regions::GameRegions>::List MenuDataProvider::GetRegionEntries(const SystemData& system)
{
  SelectorEntry<Regions::GameRegions>::List list;
  for(auto region : ViewController::Instance().GetOrCreateGamelistView((SystemData*)&system, false)->AvailableRegionsInGames())
  {
    String regionName = (region == Regions::GameRegions::Unknown) ? _("NONE") : Regions::RegionFullName(region);
    list.push_back({ regionName, region });
  }
  return list;
}

SelectorEntry<FileSorts::Sorts>::List MenuDataProvider::GetSortEntries(const SystemData& system)
{
  SelectorEntry<FileSorts::Sorts>::List list;

  // Get & check sort id
  FileSorts::SortSets set = system.IsVirtual() ? FileSorts::SortSets::MultiSystem :
                            system.Descriptor().IsArcade() ? FileSorts::SortSets::Arcade :
                            FileSorts::SortSets::SingleSystem;
  const Array<FileSorts::Sorts>& availableSorts = FileSorts::AvailableSorts(set);

  list.reserve(availableSorts.Count());
  FileSorts& sorts = FileSorts::Instance();
  for(FileSorts::Sorts sort : availableSorts)
    list.push_back({ sorts.Name(sort), sort });

  return list;
}

SelectorEntry<String>::List MenuDataProvider::GetEmulatorEntries(FileData& game, String& emulatorAndCore)
{
  SelectorEntry<String>::List list;

  String currentEmulator(game.Metadata().Emulator());
  String currentCore    (game.Metadata().Core());
  String dummy;
  MenuTools::EmulatorAndCoreList eList =
    MenuTools::ListGameEmulatorAndCore(game, dummy, dummy, currentEmulator, currentCore);
  for (const MenuTools::EmulatorAndCore& emulator : eList)
  {
    list.push_back({ emulator.Displayable, emulator.Identifier });
    if (emulator.Selected) emulatorAndCore = emulator.Identifier;
  }

  return list;
}

SelectorEntry<Path>::List MenuDataProvider::GetPatchEntries(FileData& game)
{
  SelectorEntry<Path>::List list;
  Path::PathList patches = GameFilesUtils::GetSoftPatches(&game);

  list.push_back({ _("original"), Path("original") });
  for(auto& path : patches)
  {
    String patchName = path.Directory() == game.RomPath().Directory() ? path.Filename() + " (auto)" : path.Filename();
    list.push_back({ patchName, path });
  }
  return list;
}

SelectorEntry<Path>::List MenuDataProvider::GetGameFilesToDelete(FileData& game)
{
  SelectorEntry<Path>::List list;
  list.push_back({ game.RomPath().Filename(),  game.RomPath(), false });
  for (const auto& file : GameFilesUtils::GetGameSubFiles(game))
    list.push_back({ file.Filename(), file, false });
  return list;
}

SelectorEntry<Path>::List MenuDataProvider::GetMediaFilesToDelete(FileData& game)
{
  SelectorEntry<Path>::List list;
  if (game.Metadata().Image().Exists())     list.push_back({ _("Image"), game.Metadata().Image(), false });
  if (game.Metadata().Thumbnail().Exists()) list.push_back({ _("Thumbnail"), game.Metadata().Thumbnail(), false });
  if (game.Metadata().Video().Exists())     list.push_back({ _("Video"), game.Metadata().Video(), false });
  return list;
}

SelectorEntry<Path>::List MenuDataProvider::GetExtraFilesToDelete(FileData& game)
{
  SelectorEntry<Path>::List list;
  for (const auto& path : GameFilesUtils::GetGameExtraFiles(game))
    list.push_back({ path.Filename(), path, false });
  return list;
}

SelectorEntry<Path>::List MenuDataProvider::GetSaveFilesToDelete(FileData& game)
{
  SelectorEntry<Path>::List list;
  for (const auto& path : GameFilesUtils::GetGameSaveFiles(game))
    list.push_back({ path.Filename(), path, false });
  return list;
}

SelectorEntry<Path>::List MenuDataProvider::GetPatchesEntries(FileData& game)
{
  SelectorEntry<Path>::List list;
  for(auto& path : GameFilesUtils::GetSoftPatches(&game))
  {
    String patchName = path.Directory() == game.RomPath().Directory() ? path.Filename() + " (auto)" : path.Filename();
    list.push_back({ patchName, path });
  }
  return list;
}

SelectorEntry<int>::List MenuDataProvider::GetNetplayPasswords()
{
  SelectorEntry<int>::List list { { String('(').Append(_("NONE")).Append(')'), -1 } };
  for(int i = 0; i < DefaultPasswords::sPasswordCount; i++)
  {
    String password = mConf.GetNetplayPasswords(i);
    if (password.empty()) password = DefaultPasswords::sDefaultPassword[i];
    list.push_back({ password, i });
  }
  return list;
}

