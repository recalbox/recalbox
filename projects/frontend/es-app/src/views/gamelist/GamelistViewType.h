//
// Created by bkg2k on 20/11/24.
//
#pragma once

enum class GamelistViewType
{
  Detailed, //!< Default type
  Arcade,   //!< Arcade type
};