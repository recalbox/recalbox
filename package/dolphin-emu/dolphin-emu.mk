################################################################################
#
# DOLPHIN_EMU
#
################################################################################

# Commit of 2024/09/15
DOLPHIN_EMU_SITE = https://github.com/dolphin-emu/dolphin.git
DOLPHIN_EMU_VERSION = af921685f77954713fa138b1faa1879965d2a7db
DOLPHIN_EMU_LICENSE = GPL-2.0+
DOLPHIN_EMU_LICENSE_FILES = license.txt
DOLPHIN_EMU_DEPENDENCIES = libevdev ffmpeg zlib libpng lzo libusb libcurl bluez5_utils hidapi xz host-xz
DOLPHIN_EMU_SUPPORTS_IN_SOURCE_BUILD = NO
DOLPHIN_EMU_SITE_METHOD = git
DOLPHIN_EMU_GIT_SUBMODULES = YES

DOLPHIN_EMU_CONF_OPTS += -DDISTRIBUTOR='Recalbox'
DOLPHIN_EMU_CONF_OPTS += -DENABLE_LTO=OFF
DOLPHIN_EMU_CONF_OPTS += -DUSE_DISCORD_PRESENCE=OFF
DOLPHIN_EMU_CONF_OPTS += -DTHREADS_PTHREAD_ARG=OFF
DOLPHIN_EMU_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
DOLPHIN_EMU_CONF_OPTS += -DUSE_MGBA=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_QT=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_HEADLESS=ON

ifeq ($(BR2_PACKAGE_RECALBOX_VIDEO_XORG_SERVER),y)
DOLPHIN_EMU_DEPENDENCIES += xserver_xorg-server
endif

ifeq ($(BR2_PACKAGE_RECALBOX_VIDEO_XWAYLAND),y)
DOLPHIN_EMU_DEPENDENCIES += xwayland
endif

ifeq ($(BR2_PACKAGE_RECALBOX_HAS_VULKAN),y)
DOLPHIN_EMU_DEPENDENCIES += vulkan-headers
DOLPHIN_EMU_CONF_OPTS += -DENABLE_VULKAN=ON
endif

$(eval $(cmake-package))
